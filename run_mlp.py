if __name__ == '__main__':
    import os
    import sys
    import torch
    from src.experiments.experiment_configs import experiment_configs
    from torch.utils.data import DataLoader
    from torchvision.transforms import transforms
    from src.modules.utils import train_mlp
    import torch.backends.cudnn as cudnn
    from src.modules.models import MLPClassifier, ClassificationModel
    from src.modules.data import EmbeddingDataset
    from src.modules.utils_embeddings import EmbeddingGenerator

    # Set up device
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    if device == 'cuda':
        cudnn.benchmark = True
        torch.cuda.empty_cache()

    # Get experiment configurations
    experiment_key = sys.argv[1]
    compute_embeddings = sys.argv[2] if len(sys.argv) > 2 else None

    if compute_embeddings == 'compute':
        transform = transforms.Compose([transforms.CenterCrop(256), transforms.ToTensor()])
        main_config = experiment_configs[experiment_key]
        checkpoint_path = os.path.join('checkpoints', experiment_key + '_checkpoint.pt')
        checkpoint = torch.load(checkpoint_path)
        model = ClassificationModel(**main_config['model_params'])
        model.load_state_dict(checkpoint['model_state_dict'])
        model.eval()
        train_dataset = EmbeddingDataset('img_info_data_split.csv', 'train_resized', validation=False,
                                         transform=transform)
        eval_dataset = EmbeddingDataset('img_info_data_split.csv', 'train_resized', validation=True,
                                        transform=transform)
        emb_gen = EmbeddingGenerator(model, train_dataset, main_config['batch_size'], 'embeddings/img_out',
                                     'embeddings', 'tensorboard_log', experiment_key, device, 6)
        emb_gen.store_embeddings()
        emb_gen.img_dataset = eval_dataset
        emb_gen.store_embeddings()

    config = experiment_configs[experiment_key + '_mlp']

    # Set up data and model
    training_dataset = config['dataset']('img_info_data_split.csv', os.path.join('embeddings', experiment_key),
                                         validation=False, transform=config['transform'])
    training_data_loader = DataLoader(training_dataset, batch_size=config['batch_size'], shuffle=True,
                                      num_workers=6, pin_memory=True)
    mlp = MLPClassifier(**config['model_params'])

    # Set up validation dataset
    validation_dataset = config['dataset']('img_info_data_split.csv', os.path.join('embeddings', experiment_key),
                                           validation=True, transform=config['transform'])
    validation_data_loader = DataLoader(validation_dataset, batch_size=config['batch_size'], shuffle=True,
                                        num_workers=6, pin_memory=True)

    optimizer = config['optimizer'](mlp.parameters(), lr=config['learning_rate'], **config['optimizer_params'])
    scheduler = None
    if config['scheduler']:
        scheduler = config['scheduler'](optimizer, **config['scheduler_params'])

    # Train model using the selected configurations
    train_mlp(model=mlp,
              experiment_name=experiment_key,
              training_data_loader=training_data_loader,
              validation_data_loader=validation_data_loader,
              loss=config['loss'](**config['loss_params']),
              optimizer=optimizer,
              learning_rate=config['learning_rate'],
              epochs=config['epochs'],
              scheduler=scheduler,
              device=device,
              use_checkpoint=True,
              checkpoint_folder='checkpoints'
              )
