if __name__ == '__main__':
    import os.path
    import sys
    import torch
    from src.experiments.experiment_configs import experiment_configs
    from torch.utils.data import DataLoader
    from src.modules.utils import train_model_with_mlp
    import torch.backends.cudnn as cudnn
    from src.modules.sampler import ClassBalancedBatchSampler
    from torch.utils.data.dataloader import default_collate
    from src.modules.models import ClassificationModel, MLPClassifier

    # Set up device
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    if device == 'cuda':
        cudnn.benchmark = True
        torch.cuda.empty_cache()

    # Get experiment configurations
    experiment_key = sys.argv[1]
    config = experiment_configs[experiment_key]
    main_model_config = experiment_configs[config['main_key']]

    # Get period for evaluation statistics
    eval_period = int(sys.argv[2]) if len(sys.argv) > 2 else 1

    # Load the trained model and replace FC layer by a tiny MLP
    model_checkpoint_path = os.path.join('checkpoints', config['checkpoint'])
    checkpoint = torch.load(model_checkpoint_path)
    model = ClassificationModel(**main_model_config['model_params'])
    model.load_state_dict(checkpoint['model_state_dict'])
    mlp = MLPClassifier(**config['model_params'])
    model.fc = mlp

    path_img_file = 'img_info_data_split.csv'

    # Set up training dataloader
    training_dataset = config['dataset'](path_img_file, 'train_resized', validation=False,
                                         transform=config['transform'])
    if config['sampler_params']:
        batch_sampler = ClassBalancedBatchSampler(training_dataset.img_function(), config['batch_size'],
                                                  **config['sampler_params'])
        training_data_loader = DataLoader(training_dataset, batch_sampler=batch_sampler, num_workers=6, pin_memory=True,
                                          collate_fn=default_collate, drop_last=False)
    else:
        training_data_loader = DataLoader(training_dataset, batch_size=config['batch_size'], shuffle=True,
                                          num_workers=6, pin_memory=True, prefetch_factor=4)

    # Set up validation dataset
    validation_dataset = config['dataset'](path_img_file, 'train_resized', validation=True,
                                           transform=config['transform'])
    validation_data_loader = DataLoader(validation_dataset, batch_size=config['batch_size'], shuffle=True,
                                        num_workers=6, pin_memory=True, prefetch_factor=4)

    optimizer = config['optimizer'](model.parameters(), lr=config['learning_rate'], **config['optimizer_params'])
    scheduler = None
    if config['scheduler']:
        scheduler = config['scheduler'](optimizer, **config['scheduler_params'])

    # Train model using the selected configurations
    train_model_with_mlp(model=model,
                         experiment_name=experiment_key,
                         training_data_loader=training_data_loader,
                         validation_data_loader=validation_data_loader,
                         loss=config['loss'](**config['loss_params']),
                         optimizer=optimizer,
                         learning_rate=config['learning_rate'],
                         epochs=config['epochs'],
                         scheduler=scheduler,
                         device=device,
                         use_checkpoint=True,
                         checkpoint_folder='checkpoints',
                         unfreeze_weights_at_iter=config['unfreeze_iter'],
                         eval_period=eval_period)
