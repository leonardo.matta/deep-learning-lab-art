# Deep Learning Lab - Art

## Main Task
This project is based on the Kaggle challenge [Painter by Numbers](https://www.kaggle.com/c/painter-by-numbers). Given a pair of paintings, our goal is to develop a deep neural
network that allows us to infer whether they were painted by the same artist. For our implementation, we draw inspiration from successful approaches in the field of deep metric learning. More specifically, we base our approaches on the literature of tasks such as person reidentification and information retrieval.

## Installing conda environment

The file `environment.yml` can be used to create a virtual environment with Anaconda that contains the packages we will need for the lab. To do this, simply run the following command:
```
conda env create -f environment.yml
```

## Cluster Information

Regular Login node:
```
ssh -l <uid> login18-1.hpc.itc.rwth-aachen.de
```
Interactive Login P100:
```
ssh -l <uid> login-g.hpc.itc.rwth-aachen.de
```
Interactive Login V100:
```
ssh -l <uid> login18-g-1.hpc.itc.rwth-aachen.de
```
Submit job:
```
sbatch < jobscript.sh (Please install Miniconda in your own /home/ folder for this to work)
```
Check running jobs:
```
squeue -u <uid>
```
Check project statistics:
```
r_wlm_usage -p lect0081 -q
```
To get an idea of how much physical memory your application needs, you can use the wrapper command r_memusage. We advise you to start and then stop it with CRTL+C after some seconds, because for many applications most of the memory is allocated at the beginning of their run time. Alternatively, you may use the --kill option of r_memusage. You can round up the displayed memory peak value and use it as parameter to the batch system.
```
$ r_memusage hostname
[...]
memusage: peak usage: physical memory:      0.5 MB
```

