if __name__ == '__main__':
    import sys
    import torch
    from src.experiments.experiment_configs import experiment_configs
    from torch.utils.data import DataLoader
    from src.modules.utils import train_model, train_model_triplet
    import torch.backends.cudnn as cudnn
    from src.modules.sampler import ClassBalancedBatchSampler
    from src.modules.utils_embeddings import EmbeddingGenerator
    from src.modules.data import EmbeddingDataset
    from src.modules.models import ClassificationModel
    from src.modules.losses import TripletMarginLoss

    # Set up device
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    if device == 'cuda':
        cudnn.benchmark = True
        torch.cuda.empty_cache()

    # Get experiment configurations
    experiment_key = sys.argv[1]
    config = experiment_configs[experiment_key]

    # Get period for evaluation statistics
    eval_period = int(sys.argv[2]) if len(sys.argv) > 2 else 1
    val_batch_size_multiplier = int(sys.argv[3]) if len(sys.argv) > 3 else 1
    compute_recall_for_train = True if len(sys.argv) > 4 and sys.argv[4] == 'rt' else False

    print(f'Eval period is {eval_period}. The validation batch size multiplicatorr is {val_batch_size_multiplier}. '
          f'The recall is also computed for the training set: {compute_recall_for_train}.')

    annotations_file = 'img_info_data_split.csv'

    # Set up data and model
    training_dataset = config['dataset'](annotations_file, 'train_resized', validation=False, transform=config['transform'])
    if config['sampler_params']:
        batch_sampler = ClassBalancedBatchSampler(training_dataset.img_function(), config['batch_size'], **config['sampler_params']) 
        training_data_loader = DataLoader(training_dataset, batch_sampler=batch_sampler, num_workers=6, pin_memory=True, prefetch_factor=4)
    else:
        training_data_loader = DataLoader(training_dataset, batch_size=config['batch_size'], shuffle=True, num_workers=6, pin_memory=True, prefetch_factor=4)

    model = ClassificationModel(**config['model_params'])

    # Set up validation dataset
    validation_dataset = config['dataset'](annotations_file, 'train_resized', validation=True, transform=config['transform'])
    validation_data_loader = DataLoader(validation_dataset, batch_size=val_batch_size_multiplier*config['batch_size'], shuffle=True, num_workers=6, pin_memory=True, prefetch_factor=4)

    optimizer = config['optimizer'](model.parameters(), lr=config['learning_rate'], **config['optimizer_params'])
    scheduler = None
    if config['scheduler']:
        scheduler = config['scheduler'](optimizer, **config['scheduler_params'])

    miner = None
    if config['miner']:
        miner = config['miner'](**config['miner_params'])

    triplet_loss = True if issubclass(config['loss'], torch.nn.TripletMarginLoss) or issubclass(config['loss'], TripletMarginLoss) else False

    # Train model using the selected configurations
    if triplet_loss:
        train_model_triplet(model=model,
                            experiment_name=experiment_key,
                            training_data_loader=training_data_loader,
                            validation_data_loader=validation_data_loader,
                            loss=config['loss'](**config['loss_params']),
                            optimizer=optimizer,
                            learning_rate=config['learning_rate'],
                            epochs=config['epochs'],
                            miner=miner,
                            scheduler=scheduler,
                            device=device,
                            use_checkpoint=True,
                            checkpoint_folder='checkpoints',
                            unfreeze_weights_at_iter=config['unfreeze_iter'],
                            clip_val=config['gradient_clipping_value'],
                            eval_period=eval_period,
                            train_recall=compute_recall_for_train)
    else:
        validation_embedding_dataset = EmbeddingDataset(annotations_file, 'train_resized', validation=True,
                                                        transform=config['transform'])
        emb_gen = EmbeddingGenerator(model, validation_embedding_dataset, val_batch_size_multiplier * config['batch_size'],
                                     'embeddings/img_out', 'embeddings', 'tensorboard_log', experiment_key, 'cuda', 6)
        train_model(model=model,
                    experiment_name=experiment_key,
                    training_data_loader=training_data_loader,
                    validation_data_loader=validation_data_loader,
                    embedding_generator=emb_gen,
                    loss=config['loss'](**config['loss_params']),
                    optimizer=optimizer,
                    learning_rate=config['learning_rate'],
                    epochs=config['epochs'],
                    miner=miner,
                    scheduler=scheduler,
                    device=device,
                    use_checkpoint=True,
                    checkpoint_folder='checkpoints',
                    unfreeze_weights_at_iter=config['unfreeze_iter'],
                    clip_val=config['gradient_clipping_value'],
                    eval_period=eval_period,
                    replace_fc_for_eval=config['replace_fc_for_validation'])
