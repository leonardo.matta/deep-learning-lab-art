import torch
from src.modules.utils_embeddings import EmbeddingGenerator
from src.modules.data import EmbeddingDataset
import pandas as pd
from src.experiments.experiment_configs import experiment_configs
from src.modules.models import ClassificationModel, LayerNormFeaturizer, L2NormalizedDenseLayer
from torch.nn.modules import Identity
from scipy.spatial.distance import cosine
from torchvision.models.resnet import ResNet, resnet50
from torchvision.models.efficientnet import efficientnet_v2_s
from torchvision import transforms
from sklearn import metrics
from sklearn.metrics import roc_auc_score


checkpoint_path = "checkpoints/classification_loss_7_checkpoint.pt" # change experiment number here
checkpoint = torch.load(checkpoint_path)

model = ClassificationModel(backbone=efficientnet_v2_s, featurizer=LayerNormFeaturizer, denseLayer=L2NormalizedDenseLayer,
                            fix_block_weights=True, input_norm= True) # change settings like in config file

model.load_state_dict(checkpoint['model_state_dict'])
model.fc = Identity()
model.eval()

conf_name = "classification_loss_7" # change experiment number here
conf = experiment_configs[conf_name]
experiment_name = 'classification_loss_7' # change experiment number here
annotation_file = "test_info.csv"

img_dir = "test_resized"
emb_dir = "embeddings"
img_out = "embeddings/img_out"
tensor_out = "tensorboard_log"

dataset_test_emb = EmbeddingDataset(annotation_file, img_dir, validation=False, transform=transforms.Compose([transforms.CenterCrop(256), transforms.ToTensor()]))
emb_gen = EmbeddingGenerator(model, dataset_test_emb, conf['batch_size'], img_out, emb_dir, tensor_out, experiment_name,
                             'cuda', 6)

### saving cosine similarity ###
df = pd.read_csv("submission_info.csv")
sim_metric = lambda x, y: 1 - cosine(x, y)
embeddings, labels, filenames = emb_gen.compute_embeddings()
emb_test = {file: {'emb': emb, 'label': label} for emb, label, file in zip(embeddings, labels, filenames)}
df['sameArtist'] = df.apply(lambda x: emb_test[x['image1']]['label'] == emb_test[x['image2']]['label'],
                            axis=1)  # axis1 =row-wise
df['dist'] = df.apply(lambda x: sim_metric(emb_test[x['image1']]['emb'], emb_test[x['image2']]['emb']), axis=1)

df.to_csv(r'../cosine_similarity_exp7_new.csv', index=False) # change number here

print('AUC:', metrics.roc_auc_score(df['sameArtist'], df['dist']))

submission_file = pd.DataFrame()
submission_file['index'] = df['index']
submission_file['sameArtist'] = df['dist']
submission_file['sameArtist'] *= 0.5
submission_file['sameArtist'] += 0.5
submission_file.to_csv(r'../submission_file_7_new.csv', index=False) # change experiment number here

