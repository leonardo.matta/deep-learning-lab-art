from time import time
import multiprocessing as mp
import sys
from src.experiments.experiment_configs import experiment_configs
from torch.utils.data import DataLoader


experiment_key = sys.argv[1]
config = experiment_configs[experiment_key]

training_dataset = config['dataset']('img_info_data_split.csv', 'train_resized', validation=False,
                                         transform=config['transform'])

for num_workers in range(2, mp.cpu_count(), 2):
    train_loader = DataLoader(training_dataset,shuffle=True,num_workers=num_workers,batch_size = config['batch_size'],pin_memory=True)
    start = time()
    for epoch in range(1, 3):
        for i, data in enumerate(train_loader, 0):
            pass
    end = time()
    print("Finish with:{} second, num_workers={}".format(end - start, num_workers))