import os
from tqdm import tqdm

import torch
from torch.utils.data import DataLoader
from torchvision.transforms import transforms

from src.experiments.experiment_configs import experiment_configs
from src.modules.models import MLPClassifier, ClassificationModel
from src.modules.data import MLPSubmissionDataset, EmbeddingDatasetTest
from src.modules.utils_embeddings import EmbeddingGenerator


if __name__ == '__main__':
    checkpoint_path = '../checkpoints/classification_loss_7_mlp_checkpoint.pt'
    key = 'classification_loss_7_mlp'
    config = experiment_configs[key]
    checkpoint = torch.load(checkpoint_path)

    mlp = MLPClassifier(**config['model_params'])
    mlp.load_state_dict(checkpoint['model_state_dict'])
    mlp.to('cuda')
    mlp.eval()

    submission_info = '../submission_info.csv'
    emb_dir = '../embeddings/classification_loss_7'
    submission_dataset = MLPSubmissionDataset(submission_info, emb_dir)
    dataloader = DataLoader(submission_dataset, 1024, num_workers=6, pin_memory=True, prefetch_factor=4)
    with open("mlp_submission.csv", "w") as f:
        f.write('index,sameArtist\n')
        for i, (emb_pairs, indices) in enumerate(tqdm(dataloader)):
            emb_pairs = emb_pairs.to('cuda')
            probabilities = mlp.predict_probabilities(emb_pairs).tolist()
            indices = indices.tolist()
            lines = zip(indices, probabilities)
            for l in lines:
                f.write(','.join(str(s) for s in l) + '\n')
        f.close()

