# MIT License

# Copyright (c) Microsoft Corporation. All rights reserved.
# Copyright (c) 2022 Alexander Ermolov

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE

import torch
from torch import nn
import numpy as np
import torch.nn.functional as F
from torchvision.models.resnet import resnet50
from hyptorch.nn import ToPoincare


class ClassificationModel(nn.Module):
    """
    Model consisting of a backbone, a featurrizer layer, and a dense layer.
    """

    def __init__(self, backbone=resnet50, featurizer=None, denseLayer=None, input_norm=False, pretrained=True,
                 fix_block_weights=False, dropout_p=0.0, output_dim=1584):
        super(ClassificationModel, self).__init__()

        # Initialize backbone and remove the final classification layer
        self.backbone = backbone(pretrained=pretrained)
        self.emb_dim = 0
        module_names = set(m[0] for m in self.backbone.named_children())
        for name in module_names & {'fc', 'classifier', 'head'}:
            module = self.backbone.get_submodule(name)
            if isinstance(module, nn.Sequential):
                self.emb_dim = module[-1].in_features
            else:
                self.emb_dim = module.in_features
            self.backbone.add_module(name, nn.Identity())

        # Fix backbone weights for the first epoch
        self.frozen = False
        if fix_block_weights:
            self.freeze_weights()
            self.frozen = True

        self.dropout_p = dropout_p
        self.output_dim = output_dim

        # Initialize featurizer, if provided.
        if featurizer:
            self.featurizer = featurizer(self.emb_dim, self.emb_dim, self.dropout_p)
        else:
            self.featurizer = nn.Identity()

        # Initialize dense layer, if provided. Else, initialize a simple FC layer.
        if denseLayer:
            self.fc = denseLayer(self.emb_dim, self.output_dim)
        else:
            self.fc = nn.Linear(self.emb_dim, self.output_dim)

        # Input normalization
        self.bn_input = None
        if input_norm:
            self.bn_input = nn.BatchNorm2d(3)

    def forward(self, x):
        if self.bn_input:
            x = self.bn_input(x)
        x = self.backbone(x)
        x = self.featurizer(x)
        x = self.fc(x)

        return x

    def forward_pair(self, x1, x2):
        '''
        Performs the forward pass if two images are presented to the network. Only use with an adapted (sequential)
        FC layer.
        '''
        x = torch.cat((x1, x2))  # pass through backbone as one batch
        if self.bn_input:
            x = self.bn_input(x)
        x = self.backbone(x)
        x = self.featurizer(x)
        x1, x2 = torch.chunk(x, 2)
        x = torch.cat((x1, x2), 1)
        x = self.fc(x)
        return torch.reshape(x, (-1,))

    def freeze_weights(self):
        for param in self.backbone.parameters():
            param.requires_grad = False

    def unfreeze_weights(self):
        for param in self.backbone.parameters():
            param.requires_grad = True

    @torch.inference_mode()
    def predict_probabilities(self, x1, x2):
        return torch.sigmoid(self.forward_pair(x1, x2))

    @torch.inference_mode()
    def predict(self, x1, x2):
        '''
        Predict if two images belong to the same artist.
        @param x1: first image
        @param x2: second image
        @return: 1 if the images belong to the same artist, 0 otherwise
        '''
        return torch.round(self.predict_probabilities(x1, x2))


class DropoutFeaturizer(nn.Module):
    """
    Simple featurizer that only applies dropout.
    """

    def __init__(self, input_dim, output_dim, dropout_p=0):
        super(DropoutFeaturizer, self).__init__()

        self.dropout_p = dropout_p
        self.dropout = nn.Dropout(dropout_p)

    def forward(self, x):
        if self.dropout_p > 0:
            x = self.dropout(x)

        return x


class TripletEmbeddingLayer(nn.Module):
    def __init__(self, input_dim, output_dim):
        super(TripletEmbeddingLayer, self).__init__()
        self.fc1 = nn.Linear(input_dim, output_dim)
        nn.init.kaiming_uniform_(self.fc1.weight, nonlinearity='relu')
        self.bn = nn.BatchNorm1d(output_dim)
        self.relu = nn.ReLU()
        self.fc2 = nn.Linear(output_dim, 128)
        nn.init.normal_(self.fc2.weight)

    def forward(self, x):
        x = self.fc1(x)
        x = self.bn(x)
        x = self.relu(x)
        x = self.fc2(x)
        return x


class MLPClassifier(nn.Module):
    def __init__(self, input_dim, hidden_dim):
        super(MLPClassifier, self).__init__()
        self.fc1 = nn.Linear(input_dim, hidden_dim)
        nn.init.kaiming_uniform_(self.fc1.weight, nonlinearity='relu')  # He initialization
        self.relu1 = nn.ReLU()
        self.fc2 = nn.Linear(hidden_dim, int(hidden_dim / 4))
        nn.init.kaiming_uniform_(self.fc2.weight, nonlinearity='relu')
        self.relu2 = nn.ReLU()
        self.fc3 = nn.Linear(int(hidden_dim / 4), 1)
        nn.init.xavier_uniform_(self.fc3.weight, gain=nn.init.calculate_gain('sigmoid'))  # Glorot

    def forward(self, x):
        x = self.fc1(x)
        x = self.relu1(x)
        x = self.fc2(x)
        x = self.relu2(x)
        x = self.fc3(x)
        return torch.reshape(x, (-1,))

    @torch.inference_mode()
    def predict_probabilities(self, x):
        return torch.sigmoid(self.forward(x))

    @torch.inference_mode()
    def predict(self, x):
        '''
        @param x: concatenated torch.Tensor from two embeddings of images
        @return: 1 if the images belong to the same artist, 0 otherwise
        '''
        return torch.round(self.predict_probabilities(x))


"""
The following modules implement the architecture described by the paper "Classification is a strong baseline for deep metric learning"
"""


class LayerNormFeaturizer(nn.Module):
    """
    This module is inspired by https://github.com/microsoft/computervision-recipes/blob/master/scenarios/similarity/02_state_of_the_art.ipynb
    """

    def __init__(self, input_dim, output_dim, dropout_p=0):
        super(LayerNormFeaturizer, self).__init__()

        self.layer_norm = nn.LayerNorm(input_dim, elementwise_affine=False)
        self.dropout_p = dropout_p
        self.dropout = nn.Dropout(dropout_p)

    def forward(self, x):
        # Apply layer normalization, dropout, and normalize the embedding to a unit vector
        x = self.layer_norm(x)
        if self.dropout_p > 0:
            x = self.dropout(x)
        x = F.normalize(x)

        return x


class L2NormalizedDenseLayer(nn.Module):
    """
    This module is inspired by https://github.com/microsoft/computervision-recipes/blob/master/scenarios/similarity/02_state_of_the_art.ipynb
    """

    def __init__(self, input_dim, output_dim):
        super(L2NormalizedDenseLayer, self).__init__()

        # Initialize weights the same way as nn.Linear's implementation (https://github.com/pytorch/pytorch/blob/v1.0.0/torch/nn/modules/linear.py#L129)
        self.weight = nn.Parameter(torch.Tensor(output_dim, input_dim))
        std = 1. / np.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-std, std)

    def forward(self, x):
        # Normalize the weights of the linear layer and apply it.
        normalized_weights = F.normalize(self.weight, dim=1)
        logits = F.linear(x, normalized_weights)
        return logits


"""
The following modules implement the architecture described by the paper "Hyperbolic Vision Transformers: Combining Improvements in Metric Learning".
They are inspired by the code in the paper's repository: https://github.com/htdt/hyp_metric.
"""


class L2NormalizedFeaturizer(nn.Module):
    """
    Embedding normalization for evaluation.
    """

    def __init__(self, input_dim, output_dim, dropout_p=0):
        super(L2NormalizedFeaturizer, self).__init__()

    def forward(self, x):
        # Only normalize embeddings during evaluation (model head is deactivated)
        if not self.training:
            x = F.normalize(x, p=2, dim=1)

        return x


class HypDenseLayer(nn.Module):
    """
    Embedding remapping to a lower dimension and hyperbolic projection.
    """

    def __init__(self, input_dim, output_dim, hyp_c=0.1, emb_dim=128, clip_r=2.3):
        super(HypDenseLayer, self).__init__()

        self.hyp_c = hyp_c
        self.emb_dim = emb_dim
        self.clip_r = clip_r

        self.linear = nn.Linear(input_dim, emb_dim)
        self.to_poincare = ToPoincare(c=self.hyp_c, ball_dim=self.emb_dim, riemannian=False, clip_r=self.clip_r)

        nn.init.constant_(self.linear.bias.data, 0)
        nn.init.orthogonal_(self.linear.weight.data)

    def forward(self, x):
        x = self.linear(x)
        x = self.to_poincare(x)

        return x
