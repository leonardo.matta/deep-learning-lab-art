import os
from random import randint
import pandas as pd
import torch
from PIL import Image
from torch.utils.data import Dataset

class TripletLossDataset(Dataset):
    def __init__(self, annotations_file, img_dir, validation=False, transform=None, test=False):
        if not test:
            annot_df = (pd.read_csv(annotations_file)[lambda x: x['val'] == validation])
        else:
            annot_df = pd.read_csv(annotations_file)
        self.img_df = annot_df[['filename', 'artist']]
        self.img_dir = img_dir
        self.transform = transform
        self.label2file_idx = {}
        for i, (file, label) in enumerate(zip(self.img_df['filename'], self.img_df['artist'])):
            if label in self.label2file_idx:
                self.label2file_idx[label].append(i)
            else:
                self.label2file_idx[label] = [i]

    def __len__(self):
        return len(self.img_df)

    def __getitem__(self, idx_anchor):
        label_anchor = self.img_df.iat[idx_anchor, 1]
        positives = self.label2file_idx[label_anchor]
        while True:
            rnd_sample = randint(0, len(positives) - 1)
            idx_positive = positives[rnd_sample]
            if idx_positive != idx_anchor or len(positives) < 2:
                break
        while True:
            idx_negative = randint(0, self.__len__() - 1)
            if idx_negative not in positives:
                break
        path_anchor = os.path.join(self.img_dir, self.img_df.iat[idx_anchor, 0])
        path_positive = os.path.join(self.img_dir, self.img_df.iat[idx_positive, 0])
        path_negative = os.path.join(self.img_dir, self.img_df.iat[idx_negative, 0])
        with open(path_anchor, 'rb') as f:
            anchor = Image.open(f).convert('RGB')
        with open(path_positive, 'rb') as f:
            positive = Image.open(f).convert('RGB')
        with open(path_negative, 'rb') as f:
            negative = Image.open(f).convert('RGB')

        label_positive = self.img_df.iat[idx_positive, 1]
        label_negative = self.img_df.iat[idx_negative, 1]
        if self.transform:
            anchor = self.transform(anchor)
            positive = self.transform(positive)
            negative = self.transform(negative)
        return anchor, positive, negative, label_anchor, label_positive, label_negative

    def img_function(self):
        image_labels = {}
        for idx in range(len(self.img_df)):
            label = self.img_df.iloc[idx, 1]
            image_labels[idx] = label
        return image_labels


class ClassificationLossDataset(Dataset):
    def __init__(self, annotations_file, img_dir, validation=False, transform=None, target_transform=None,
                 augmentation=None):
        self.img_labels = pd.read_csv(annotations_file)
        self.img_labels = self.img_labels[self.img_labels['val'] == validation]
        self.img_dir = img_dir
        self.transform = transform
        self.target_transform = target_transform
        self.augmentation = augmentation

    def __len__(self):
        return len(self.img_labels)

    def __getitem__(self, idx):
        img_path = os.path.join(self.img_dir, str(self.img_labels.iloc[idx, 0]))
        image = None
        with open(img_path, 'rb') as f:
            image = Image.open(f).convert('RGB')
        label = self.img_labels.iloc[idx, 1]
        if self.augmentation:
            image = self.augmentation(image)
        if self.transform:
            image = self.transform(image)
        if self.target_transform:
            label = self.target_transform(label)
        return image, label

    def img_function(self):
        image_labels = {}
        for idx in range(len(self.img_labels)):
            label = self.img_labels.iloc[idx, 1]
            image_labels[idx] = label
        return image_labels


class BCELossDataset(Dataset):
    '''
    Returns two images with equal probability of belonging to the same class.
    For use with train, validation and test-set.
    '''

    def __init__(self, annotations_file, img_dir, validation=False, transform=None, test=False):
        if not test:
            self.annot_df = (pd.read_csv(annotations_file)[lambda x: x['val'] == validation])
        else:
            self.annot_df = pd.read_csv(annotations_file)
        self.img_df = self.annot_df[['filename', 'artist']]
        self.label2file_idx = {}
        for i, (file, label) in enumerate(zip(self.img_df['filename'], self.img_df['artist'])):
            if label in self.label2file_idx:
                self.label2file_idx[label].append(i)
            else:
                self.label2file_idx[label] = [i]
        self.img_dir = img_dir
        self.transform = transform
        # self.images = {}
        # for i, file in enumerate(self.img_df['filename']):
        #     img_path = os.path.join(self.img_dir, self.img_df.iat[i, 0])
        #     with open(img_path, 'rb') as f:
        #         if transform:
        #             self.images[i] = transform(Image.open(f).convert('RGB'))
        #         else:
        #             self.images[i] = Image.open(f).convert('RGB')
        # print('Loading Dataset completed.')

    def __len__(self):
        return len(self.img_df)

    def __getitem__(self, idx):
        img1_path = os.path.join(self.img_dir, str(self.img_df.iat[idx, 0]))
        with open(img1_path, 'rb') as f:
            img1 = Image.open(f).convert('RGB')
        label1 = self.img_df.iat[idx, 1]
        class_samples = self.label2file_idx[label1]
        same_class = randint(0, 1)
        if same_class:
            while True:
                rnd_idx = randint(0, len(class_samples) - 1)
                sample_idx = class_samples[rnd_idx]
                if sample_idx != idx or len(class_samples) == 1:
                    break
        else:
            while True:
                sample_idx = randint(0, self.__len__() - 1)
                if sample_idx not in class_samples:
                    break
        img2_path = os.path.join(self.img_dir, str(self.img_df.iat[sample_idx, 0]))
        with open(img2_path, 'rb') as f:
            img2 = Image.open(f).convert('RGB')
        if self.transform:
            img1 = self.transform(img1)
            img2 = self.transform(img2)
        return img1, img2, torch.as_tensor(same_class, dtype=torch.float32)


class EmbeddingDataset(Dataset):
    def __init__(self, annotations_file, img_dir, validation=True, transform=None, target_transform=None,
                 augmentation=None):
        self.img_labels = pd.read_csv(annotations_file)
        self.img_labels = self.img_labels[self.img_labels['val'] == validation]
        self.img_dir = img_dir
        self.transform = transform
        self.target_transform = target_transform
        self.augmentation = augmentation

    def __len__(self):
        return len(self.img_labels)

    def __getitem__(self, idx):
        img_path = os.path.join(self.img_dir, str(self.img_labels.iloc[idx, 0]))
        image = None
        with open(img_path, 'rb') as f:
            image = Image.open(f).convert('RGB')
        label = self.img_labels.iloc[idx, 1]
        if self.augmentation:
            image = self.augmentation(image)
        if self.transform:
            image = self.transform(image)
        if self.target_transform:
            label = self.target_transform(label)
        return image, label, str(self.img_labels.iloc[idx, 0])


class BCELossMLPDataset(Dataset):
    '''
    Returns with equal probability of belonging to the same class a concatenated tensor of two embeddings.
    '''

    def __init__(self, annotations_file, emb_dir, validation=False, transform=None):
        self.annot_df = (pd.read_csv(annotations_file)[lambda x: x['val'] == validation])
        self.emb_df = self.annot_df['filename'].apply(lambda x: str(x).rsplit('.')[0] + '.pt')
        self.label2file_idx = {}
        for i, emb in enumerate(zip(self.emb_df, self.annot_df['artist'])):
            label = emb[1]
            if label in self.label2file_idx:
                self.label2file_idx[label].append(i)
            else:
                self.label2file_idx[label] = [i]
        self.emb_dir = emb_dir
        self.transform = transform

    def __len__(self):
        return len(self.emb_df)

    def __getitem__(self, idx):
        emb1_path = os.path.join(self.emb_dir, self.emb_df.iloc[idx])
        emb1, label1 = torch.load(emb1_path)
        class_samples = self.label2file_idx[label1.item()]
        same_class = randint(0, 1)
        if same_class:
            while True:
                rnd_idx = randint(0, len(class_samples) - 1)
                sample_idx = class_samples[rnd_idx]
                if sample_idx != idx or len(class_samples) == 1:
                    break
        else:
            while True:
                sample_idx = randint(0, self.__len__() - 1)
                if sample_idx not in class_samples:
                    break
        emb2_path = os.path.join(self.emb_dir, self.emb_df.iloc[sample_idx])
        emb2, label2 = torch.load(emb2_path)
        if self.transform:
            emb1 = self.transform(emb1)
            emb2 = self.transform(emb2)
        return torch.cat((emb1, emb2)), torch.as_tensor(same_class, dtype=torch.float32)


class MLPSubmissionDataset(Dataset):
    def __init__(self, submission_file, emb_dir, transform=None):
        self.df_submission = pd.read_csv(submission_file, index_col=0)
        self.data = self.df_submission.applymap(lambda x: str(x).rsplit('.')[0] + '.pt')
        self.emb_dir = emb_dir
        self.transform = transform
        print(self.data.head())

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        emb1_path = os.path.join(self.emb_dir, self.data.iat[idx, 0])
        emb1, label1 = torch.load(emb1_path)
        emb2_path = os.path.join(self.emb_dir, self.data.iat[idx, 1])
        emb2, label2 = torch.load(emb2_path)
        if self.transform:
            emb1 = self.transform(emb1)
            emb2 = self.transform(emb2)
        return torch.cat((emb1, emb2)), idx

class SimilarityDataset(Dataset):
    '''
    Returns every pair of embeddings.
    '''
    def __init__(self, embs1, embs2):
        self.embs1 = embs1
        self.embs2 = embs2
        idx1, idx2 = np.meshgrid(range(len(embs1)), range(len(embs2)))
        self.idx1, self.idx2 = idx1.flatten(), idx2.flatten()

    def __len__(self):
        return len(self.embs1) * len(self.embs2)

    def __getitem__(self, idx):
        return torch.cat((self.embs1[self.idx1[idx]], self.embs2[self.idx2[idx]]))
