import torch


class TripletMarginMiner(torch.nn.Module):
    """ This miner class is based on https://github.com/KevinMusgrave/pytorch-metric-learning.
        Returns triplets that violate the margin accordingly to the specified triplet type.

        @param margin: the margin that should be valid between an anchor positive and anchor negative triplet.
        @param type_of_triplets: options are "all", "hard", "semihard" and "easy":
                            "all" means all possible triplets within the batch
                            "easy" is a subset of "all", containing only the triplets that violate the margin
                            "semihard" is a subset of "easy", but the negative is further from the anchor than the positive
                            "hard" is a subset of "easy", but the negative is closer to the anchor than the positive
                            "batch-hard" from the paper In Defense of the Triplet Loss for Person Re-Identification
        @param verbose: save information regarding the batch. If the chosen triplet type is not "batch hard", then "active_fraction" is the ration between all possible triplets and the selected triplets.
                                                              "pos_pair_dist", "neg_pair_dist" gives the distances between possible anchor-positive and anchor-negative pairs.
                                                              "avg_triplet_margin" is the distance between anchor-negative and anchor-positive triplets.
                                                              If it is "batch hard", "active_fraction" and "avg_triplet_margin" are not computed.
    """

    def __init__(self, margin=0.2, type_of_triplets="all", verbose=False):
        super(TripletMarginMiner, self).__init__()
        self.margin = margin
        self.type_of_triplets = type_of_triplets
        self.verbose = verbose
        self.active_fraction = 0
        self.pos_pair_dist = 0
        self.neg_pair_dist = 0
        self.avg_triplet_margin = 0

    def get_batch_info(self):
        names = ["fraction_of_active_triplets", "mean_anchor-posivitive_distance", "mean_anchor-negative_dist", "mean_triplet_margin"]
        values = [self.active_fraction, self.pos_pair_dist, self.neg_pair_dist, self.avg_triplet_margin]
        return dict(zip(names, values))

    def get_matches_and_diffs(self, labels, fill_diagonal=True):
        labels1 = labels.unsqueeze(1)
        labels2 = labels.unsqueeze(0)
        matches = (labels1 == labels2).byte()
        diffs = matches ^ 1
        if fill_diagonal:
            matches.fill_diagonal_(0)
        return matches, diffs

    def get_all_triplets_indices(self, labels):
        matches, diffs = self.get_matches_and_diffs(labels)
        triplets = matches.unsqueeze(2) * diffs.unsqueeze(1)
        return torch.where(triplets)

    @torch.no_grad()
    def mine(self, embeddings, labels):
        dist_mat = torch.cdist(embeddings, embeddings)
        if self.type_of_triplets == "batch-hard":
            matches, diffs = self.get_matches_and_diffs(labels, False)
            anchor_idx = torch.arange(labels.size(0))
            anchor_positive_dist_mat = matches * (dist_mat + 1e-16)  # assert that if an image is resampled and therefore has dist of zero, it is still chosen as positive over one with different label
            positive_idx = torch.topk(anchor_positive_dist_mat, 1, 0)[1].squeeze()
            anchor_negative_dist_mat = dist_mat * diffs
            anchor_negative_dist_mat[anchor_negative_dist_mat <= 0] = float('inf')  # anchor_negative_dist_mat = anchor_negative_dist_mat + matches * 1e6
            negative_idx = torch.topk(anchor_negative_dist_mat, 1, 0, False)[1].squeeze()

            if self.verbose:
                self.pos_pair_dist = (torch.sum(anchor_positive_dist_mat) / torch.count_nonzero(anchor_negative_dist_mat)).item()
                self.neg_pair_dist = (torch.sum(torch.nan_to_num(anchor_negative_dist_mat, posinf=0)) / torch.count_nonzero(anchor_negative_dist_mat)).item()
                self.avg_triplet_margin = float('nan')
                self.active_fraction = float('nan')
            return anchor_idx, positive_idx, negative_idx

        else:
            anchor_idx, positive_idx, negative_idx = self.get_all_triplets_indices(labels)
            anchor_positive_dist = dist_mat[anchor_idx, positive_idx]
            anchor_negative_dist = dist_mat[anchor_idx, negative_idx]
            triplet_margin = anchor_negative_dist - anchor_positive_dist

            if self.type_of_triplets == "all":
                threshold_condition = torch.ones_like(anchor_idx).bool()

            else:
                threshold_condition = triplet_margin <= self.margin  # self.type_of_triplets == "easy"
                if self.type_of_triplets == "semihard":
                    threshold_condition &= triplet_margin > 0
                elif self.type_of_triplets == "hard":
                    threshold_condition &= triplet_margin <= 0

            if self.verbose:
                self.pos_pair_dist = torch.mean(anchor_positive_dist).item()
                self.neg_pair_dist = torch.mean(anchor_negative_dist).item()
                self.avg_triplet_margin = torch.mean(triplet_margin).item()
                self.active_fraction = (torch.count_nonzero(threshold_condition) / anchor_idx.size(0)).item()

            return anchor_idx[threshold_condition], positive_idx[threshold_condition], negative_idx[threshold_condition]
