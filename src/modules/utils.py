import torch
import wandb
import os

import faiss
import numpy as np
import pandas as pd
import torch
import wandb
from fastprogress import master_bar, progress_bar
from sklearn.metrics import accuracy_score, roc_auc_score, precision_recall_fscore_support
from sklearn.metrics.pairwise import cosine_similarity, euclidean_distances
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.utils.data import DataLoader
from tqdm import tqdm

from hyptorch.pmath import dist_matrix
from src.modules.losses import TripletMarginLoss
from tqdm import tqdm
from src.modules.models import HypDenseLayer
from src.modules.data import SimilarityDataset

def retrieve_knn(embeddings, k, gpu_id, cosine=True):
    """
        (This function was taken from the implementation by Zhai et al. in https://github.com/azgo14/classification_metric_learning/blob/master/evaluation/retrieval.py)
        Retrieve k nearest neighbor based on inner product
        Args:
            query_embeddings:           numpy array of size [NUM_QUERY_IMAGES x EMBED_SIZE]
            db_embeddings:              numpy array of size [NUM_DB_IMAGES x EMBED_SIZE]
            k:                          number of nn results to retrieve excluding query
            gpu_id:                     gpu device id to use for nearest neighbor (if possible for `metric` chosen)
            cosine:                     if True knn based on cosine, else on euclidean distance
        Returns:
            dists:                      numpy array of size [NUM_QUERY_IMAGES x k], distances of k nearest neighbors
                                        for each query
            retrieved_db_indices:       numpy array of size [NUM_QUERY_IMAGES x k], indices of k nearest neighbors
                                        for each query
    """

    res = faiss.StandardGpuResources()
    flat_config = faiss.GpuIndexFlatConfig()
    flat_config.device = gpu_id

    if cosine:
        # Evaluate with inner product
        index = faiss.GpuIndexFlatIP(res, embeddings.shape[1], flat_config)
    else:
        # Evaluate with euclidean dist
        index = faiss.GpuIndexFlatL2(res, embeddings.shape[1], flat_config)

    index.add(embeddings)

    _, result_indices = index.search(embeddings, k + 1)

    return result_indices


def faiss_evaluate_recall_at_k(embeddings, labels, k, gpu_id=0, cosine=True):
    """
        (This function was taken and adapted from the implementation by Zhai et al. in https://github.com/azgo14/classification_metric_learning/blob/master/evaluation/retrieval.py)
        Evaluate Recall@k based on retrieval results
        Args:
            results:        numpy array of size [NUM_QUERY_IMAGES x k], indices of k nearest neighbors for each query
            query_labels:   list of labels for each query
            db_labels:      list of labels for each db
            k:              number of nn results to evaluate
            cosine:         if True knn based on cosine, else on euclidean distance

        Returns:
            recall_at_k:    Recall@k in percentage
    """
    results = retrieve_knn(embeddings, k, gpu_id, cosine)

    recall_at_k = np.zeros((k,))

    for i in range(len(labels)):
        pos = 0  # keep track recall at pos
        j = 0  # looping through results
        while pos < k:
            if i == results[i, j]:
                # Only skip the document when query and index sets are the exact same
                j += 1
                continue
            if labels[i] == labels[results[i, j]]:
                recall_at_k[pos:] += 1
                break
            j += 1
            pos += 1

    return recall_at_k / float(len(labels))


def cos_sim_metric(x, y):
    """
    Returns the matrix cosine similarities between all embeddings.
    """
    return 0.5 + cosine_similarity(x, y)*0.5

def l2_sim_metric(x, y):
    """
    Compute euclidean distances between embeddings, normalize them between 0 and 1, and invert them to obtain similarity score.
    """
    dist_m = euclidean_distances(x, y)
    # min_dist = dist_m.min()
    # max_dist = dist_m.max()
    # dist_m = (dist_m - min_dist)/(max_dist - min_dist)

    return - dist_m

def hyp_sim_metric(x, y, hyp_c=0.1):
    distance_matrix = torch.empty(len(x), len(y), device='cuda')
    x = torch.Tensor(x).to('cuda')
    y = torch.Tensor(y).to('cuda')
    for i in tqdm(range(len(x))):
        distance_matrix[i:i + 1] = -dist_matrix(x[i:i + 1], y, hyp_c)

    distance_matrix = distance_matrix.to('cpu')

    return distance_matrix.numpy()


def mlp_sim_metric(x, y, mlp):
    """
    Returns a matrix of probabilities for every image pair.
    """
    dataset = SimilarityDataset(x, y)
    data_loader = DataLoader(dataset, batch_size=len(y), pin_memory=True, num_workers=6)
    sim_matrix = np.empty((len(x), len(y)))

    mlp.eval()
    mlp.to('cuda')
    for i, emb in tqdm(enumerate(iter(data_loader))):
        emb = emb.to('cuda')

        output = mlp.predict_probabilities(emb)
        output = output.to('cpu')
        output = torch.flatten(output)
        sim_matrix[i, :] = output.numpy()

    return sim_matrix

def evaluate_recall_at_k(query_embeddings, db_embeddings, query_labels, db_labels, k, sim_metric=cos_sim_metric, sim_matrix=None, gpu_id=0):
    """
    Returns the R@K metric for the given embeddings.
    """
    
    if sim_matrix is None:
        sim_matrix = sim_metric(query_embeddings, db_embeddings)

    knn_idx = np.argsort(sim_matrix, axis=1)[:, -k-1:-1]
    print(np.sort(sim_matrix, axis=1)[:, -k-1:-1])
    knn_labels = db_labels[knn_idx]
    #knn_idx = retrieve_knn(query_embeddings, db_embeddings, k, gpu_id)

    recall_at_k = 0
    for i in range(len(query_labels)):
        if query_labels[i] in knn_labels[i]:
            recall_at_k += 1
    
    return recall_at_k / len(query_labels)


@torch.no_grad()
def test_model(model, data_loader, loss, pred_type='softmax', img_pairs=False):
    """
    Evaluate model on a given data set and return predictions and loss.

    @param model: Model to be evaluated
    @param data_loader: Data used to evaluate the model
    @param loss: Criterion for training the model
    @type pred_type: 'softmax' or 'sigmoid' depending on the model
    @type img_pairs: Flag if one or two images are loaded
    """

    pred = []
    true = []
    running_loss = 0
    n_iter = 0
    if img_pairs:
        for img1, img2, labels in iter(data_loader):
            if next(model.parameters()).is_cuda:
                img1, img2, labels = img1.cuda(), img2.cuda(), labels.cuda()
            output = model.forward_pair(img1, img2)
            true += labels.tolist()
            pred += torch.round(torch.sigmoid(output)).tolist()
            running_loss += loss(output, labels)
            n_iter += 1

    else:
        for inputs, labels in iter(data_loader):
            if next(model.parameters()).is_cuda:
                inputs = inputs.to('cuda')
                labels = labels.to('cuda')
            output = model(inputs)

            true += labels.tolist()
            if pred_type == 'softmax':
                pred += torch.argmax(output, dim=1).tolist()
            else:
                output = output.squeeze()
                pred += torch.round(torch.sigmoid(output)).tolist()
            running_loss += loss(output, labels)
            n_iter += 1

    return true, pred, running_loss / n_iter

@torch.no_grad()
def test_verification_performance(embs, labels, filenames, pairs_file, sim_metric=cos_sim_metric, save_plot=True, save_submission=False,
                                  submission_name='submission_file.csv'):
    """
    Test the final model performance on the verification task. We first precompute the embeddings
    using the model to be evaluated, then we compute a score for each pair based on the similarity
    between the embeddings of the images. Finally, we return the AUC based on the resulting scores.

    @param emb_gen: EmbeddingGenerator object for the test set.
    @param pairs_file: .csv file containg the image pairs to be evaluated
    @param sim_metric: Similarity metric to compare embeddings
    """

    df = pd.read_csv(pairs_file)

    #embs, labels, filenames = emb_gen.compute_embeddings()
    emb_test = {file: {'emb': emb, 'label': label} for emb, label, file in zip(embs, labels, filenames)}

    #idx = {file: i for i, file in enumerate(filenames)}
    # sim_matrix = sim_metric(embs, embs)
    # sim_matrix = np.minimum(sim_matrix, 1.0)
    df['positive'] = df.apply(lambda x: emb_test[x['image1']]['label'] == emb_test[x['image2']]['label'], axis=1)
    df['sameArtist'] = df.apply(lambda x: - np.linalg.norm(emb_test[x['image1']]['emb'] - emb_test[x['image2']]['emb']), axis=1)

    if save_submission:
        df.to_csv(submission_name, index=False)

    return roc_auc_score(df['positive'], df['sameArtist'])

@torch.no_grad()
def test_verification_mlp(model, embs, labels, filenames, pairs_file, save_plot=True, save_submission=False,
                                  submission_name='submission_file.csv'):
    """
    Test the final model performance on the verification task using an MLP.

    @param model: MLP model to be used for the verification task
    @param emb_gen: EmbeddingGenerator object for the test set.
    @param pairs_file: .csv file containg the image pairs to be evaluated
    @param sim_metric: Similarity metric to compare embeddings
    """

    df = pd.read_csv(pairs_file)

    #embs, labels, filenames = emb_gen.compute_embeddings()
    emb_test = {file: {'emb': torch.unsqueeze(torch.Tensor(emb), 0), 'label': label} for emb, label, file in zip(embs, labels, filenames)}
    #pairs = [(x[1]['image1'], x[1]['image2']) for x in tqdm(df.iterrows())]
    outputs = []
    model.eval()
    model.to('cuda')
    for i in tqdm(range(int(np.ceil(len(df) / 20000)))):
        input = torch.cat([torch.cat((emb_test[x['image1']]['emb'], emb_test[x['image2']]['emb']), -1) for _, x in list(df.iterrows())[i:i+20000]], 0)
        input = input.to('cuda')
        output = model(input)
        output = torch.flatten(output.cpu()).tolist()
        outputs += output

    
    df['positive'] = df.apply(lambda x: emb_test[x['image1']]['label'] == emb_test[x['image2']]['label'], axis=1)
    df['sameArtist'] = outputs

    if save_submission:
        df[['index', 'sameArtist']].to_csv(submission_name, index=False)

    return roc_auc_score(df['positive'], df['sameArtist'])


def train_model(model, experiment_name, training_data_loader, validation_data_loader, embedding_generator, loss,
                optimizer, learning_rate, epochs, miner=None,
                scheduler=None, device='cpu', use_checkpoint=True, checkpoint_folder='',
                unfreeze_weights_at_iter=None, clip_val=None, eval_period=1, replace_fc_for_eval=True):
    """
    Train the model for the specified amount of epochs.

    @param model: Model to be trained.
    @param training_data_loader: DataLoader for the training data.
    @param validation_data_loader: DataLoader for the validation data.
    @param embedding_generator: Class for storing and loading image embeddings
    @param loss: Loss function used for the training.
    @param optimizer: Optimizer used during training.
    @param learning_rate: Learning rate used during training.
    @param epochs: Number of epochs for which to train the model.
    @param scheduler: Scheduler used to tune the learning rate during training.
    @param miner: Miner used for triplet mining.
    @param device: Device on which to load the data and the model (CPU vs. GPU).
    @param use_checkpoint: Set to true if the training run should start at the last saved checkpoint (if it exists).
    @param checkpoint_folder: Folder in which to store the checkpoint file.
    @param eval_functions: Functions used to evaluate the model on the training and validation sets.
    @param unfreeze_weights_at_iter: Specifies in which epoch the backbone weights are unfrozen.
    @param clip_val:
    @param eval_period:
    @param replace_fc_for_eval: Specifies if the classification layer should be dropped or not.

    """

    # Create experiment name
    backbone_name = model.backbone.__class__.__name__
    loss_name = loss.__class__.__name__
    optimizer_name = optimizer.__class__.__name__
    scheduler_name = scheduler.__class__.__name__ if scheduler else 'NoScheduler'
    checkpoint_path = os.path.join(checkpoint_folder, f'{experiment_name}_checkpoint.pt')
    triplet_type = "mode: " + miner.type_of_triplets if miner else 'NoMiner'

    # os.environ['WANDB_MODE'] = 'offline'
    wandb.login(key="")
    with wandb.init(project="dll-experiments", entity="dll-art", resume=True, name=experiment_name,
                    tags=[backbone_name, loss_name, optimizer_name, scheduler_name, str(learning_rate),
                          triplet_type]) as run:

        # wandb watches gradients and parameters
        wandb.watch(model, log="all", log_freq=100)

        # Run information
        run.config.learning_rate = learning_rate
        run.config.optimizer = optimizer_name
        run.watch(model)

        # Set model to training mode
        if device == 'cuda':
            model.to(device)
        model.train()

        # Check if checkpoint exists. If it does, load it.
        start_epoch = 1
        if os.path.isdir(checkpoint_folder):
            if os.path.isfile(checkpoint_path) and use_checkpoint:
                checkpoint = torch.load(checkpoint_path)
                model.load_state_dict(checkpoint['model_state_dict'])
                optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
                start_epoch = checkpoint['epoch'] + 1
                learning_rate = optimizer.param_groups[0]['lr']
        else:
            os.makedirs(checkpoint_folder)

        mb = master_bar(range(start_epoch, epochs + 1))
        for epoch in mb:

            # Unfreeze backbone weights if necessary
            if epoch >= unfreeze_weights_at_iter and model.frozen:
                model.unfreeze_weights()

            # Here 'inputs' can refer to a list of single images, pairs, or triples (depending on loss function)
            n_minibatches = 0
            running_loss = 0
            true = []
            pred = []
            for inputs, labels in progress_bar(iter(training_data_loader), parent=mb):

                # Move batch to GPU (if it is available)
                if device == 'cuda':
                    inputs, labels = inputs.to(device), labels.to(device)

                # Set gradients to zero
                optimizer.zero_grad(set_to_none=True)

                # Perform the forward pass
                outputs = model(inputs)
                # Compute batch loss and backpropagate
                batch_loss = loss(outputs, labels)
                batch_loss.backward()
                
                # Clip gradients if necessary
                if clip_val:
                    torch.nn.utils.clip_grad_norm(model.parameters(), clip_val)

                # Clip gradients if necessary
                if clip_val:
                    torch.nn.utils.clip_grad_norm(model.parameters(), clip_val)

                # Perform optimization step
                optimizer.step()

                # Update predicted and true labels for later statistics
                true += labels.tolist()
                pred += torch.argmax(outputs, dim=1).tolist()

                # Update running loss
                n_minibatches += 1
                running_loss += batch_loss.item()

            # Only log statistics every eval_period epochs
            if epoch % eval_period == 0 or epoch == epochs:
                model.eval()

                # Compute and load validation set embeddings
                embeddings, emb_labels, _ = embedding_generator.compute_embeddings(disable=replace_fc_for_eval)
                # Log model statistics
                if isinstance(model.fc, HypDenseLayer):
                    embeddings = torch.Tensor(embeddings)
                    recall_at_1 = evaluate_recall_at_k(embeddings, embeddings, emb_labels, 1, 0, metric=hyp_sim_metric)
                else:
                    recall_at_1 = evaluate_recall_at_k(embeddings, embeddings, emb_labels, 1, 0)
                running_loss = running_loss / n_minibatches
                train_accuracy = accuracy_score(true, pred)
                true_val, pred_true, val_loss = test_model(model, validation_data_loader, loss)
                val_accuracy = accuracy_score(true_val, pred_true)
                run.log({
                    'epoch': epoch,
                    'train_loss': running_loss,
                    'validation_loss': val_loss,
                    'learning_rate': learning_rate,
                    'validation_recall_at_1': recall_at_1,
                    'train_accuracy': train_accuracy,
                    'validation_accuracy': val_accuracy
                })

            model.train()

            # Perform scheduler step (if necessary)
            if scheduler:
                learning_rate = optimizer.param_groups[0]['lr']
                scheduler.step(running_loss)

            # Save checkpoint after each epoch
            torch.save({
                'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict()
            }, checkpoint_path)


def train_model_triplet(model, experiment_name, training_data_loader, validation_data_loader, loss,
                        optimizer, learning_rate, epochs, miner=None,
                        scheduler=None, device='cpu', use_checkpoint=True, checkpoint_folder='',
                        unfreeze_weights_at_iter=None, clip_val=None, eval_period=1, train_recall=False):
    """
    Train the model for the specified amount of epochs.

    @param model: Model to be trained.
    @param experiment_name: Name/Key of the experiment
    @param training_data_loader: DataLoader for the training data.
    @param validation_data_loader: DataLoader for the validation data.
    @param loss: Loss function used for the training.
    @param optimizer: Optimizer used during training.
    @param learning_rate: Learning rate used during training.
    @param epochs: Number of epochs for which to train the model.
    @param scheduler: Scheduler used to tune the learning rate during training.
    @param miner: Miner used for triplet mining.
    @param device: Device on which to load the data and the model (CPU vs. GPU).
    @param use_checkpoint: Set to true if the training run should start at the last saved checkpoint (if it exists).
    @param checkpoint_folder: Folder in which to store the checkpoint file.
    @param unfreeze_weights_at_iter: Specifies in which epoch the backbone weights are unfrozen.
    @param clip_val:
    @param eval_period:
    @param train_recall: True if recall@k should also be computed on batch-level for the training data. Uses only every second batch.
    """

    # Create experiment name
    backbone_name = model.backbone.__class__.__name__
    loss_name = loss.__class__.__name__
    optimizer_name = optimizer.__class__.__name__
    scheduler_name = scheduler.__class__.__name__ if scheduler else 'NoScheduler'
    checkpoint_path = os.path.join(checkpoint_folder, f'{experiment_name}_checkpoint.pt')
    triplet_type = "mode: " + miner.type_of_triplets if miner else 'NoMiner'

    # os.environ['WANDB_MODE'] = 'offline'
    wandb.login(key="")
    with wandb.init(project="dll-experiments", entity="dll-art", resume=True, name=experiment_name,
                    tags=[backbone_name, loss_name, optimizer_name, scheduler_name, str(learning_rate),
                          triplet_type]) as run:

        # wandb watches gradients and parameters
        wandb.watch(model, log="all", log_freq=100)

        # Run information
        run.config.learning_rate = learning_rate
        run.config.optimizer = optimizer_name
        run.watch(model)

        # Set model to training mode
        if device == 'cuda':
            model.to(device)
        model.train()

        # Check if checkpoint exists. If it does, load it.
        start_epoch = 1
        if os.path.isdir(checkpoint_folder):
            if os.path.isfile(checkpoint_path) and use_checkpoint:
                checkpoint = torch.load(checkpoint_path)
                model.load_state_dict(checkpoint['model_state_dict'])
                optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
                start_epoch = checkpoint['epoch'] + 1
                learning_rate = optimizer.param_groups[0]['lr']
        else:
            os.makedirs(checkpoint_folder)

        vanilla_triplet_loss = False if miner else True
        assert vanilla_triplet_loss == isinstance(loss, torch.nn.TripletMarginLoss), 'incompatible selection of loss and miner'

        mb = master_bar(range(start_epoch, epochs + 1))
        mb.main_bar.comment = 'Epoch'
        for epoch in mb:

            # Unfreeze backbone weights if necessary
            if epoch >= unfreeze_weights_at_iter and model.frozen:
                model.unfreeze_weights()

            n_minibatches = 0
            running_loss = 0
            running_batch_recall = []
            # start training loop
            for i, inputs_and_labels in enumerate(progress_bar(training_data_loader, parent=mb)):
                mb.child.comment = 'Training'

                if vanilla_triplet_loss:
                    inputs = torch.cat(inputs_and_labels[:3])  # anchor, positive and negative
                    labels = torch.cat(inputs_and_labels[3:])  # corresponding labels
                else:
                    inputs, labels = inputs_and_labels

                # Move batch to GPU (if it is available)
                if device == 'cuda':
                    if vanilla_triplet_loss:
                        inputs = inputs.to(device)
                    else:
                        inputs, labels = inputs.to(device), labels.to(device)

                # Set gradients to zero
                optimizer.zero_grad(set_to_none=True)

                # Perform the forward pass
                outputs = model(inputs)
                if not vanilla_triplet_loss:
                    if (i + 1) % 100 == 0 or (i + 1) == len(training_data_loader):  # log sparsely information regarding the mined triplets
                        miner.verbose = True
                        indices = miner.mine(outputs, labels)
                        run.log(miner.get_batch_info(), commit=False)
                        miner.verbose = False
                    else:
                        indices = miner.mine(outputs, labels)
                    batch_loss = loss(outputs, indices)  # custom TripletMarginLoss
                else:
                    batch_loss = loss(*torch.chunk(outputs, 3))  # torch.nn.TripletMarginLoss
                # Compute batch loss and backpropagate
                batch_loss.backward()

                # Clip gradients if necessary
                if clip_val:
                    torch.nn.utils.clip_grad_norm(model.parameters(), clip_val)

                # Perform optimization step
                optimizer.step()

                # Update running loss
                n_minibatches += 1
                running_loss += batch_loss.item()

                if (epoch % eval_period == 0 or epoch == epochs) and train_recall:
                    if i % 2 == 0:  # consider only every second batch
                        running_batch_recall.append(evaluate_recall_at_k(outputs.cpu().detach().numpy(), labels.cpu().numpy(), 5, 0, False))

            running_loss = running_loss / n_minibatches

            # Only log certain statistics every eval_period epochs
            extended_log = {}
            if epoch % eval_period == 0 or epoch == epochs:
                model.eval()
                with torch.no_grad():

                    embeddings_val = []
                    labels_val = []
                    for inputs_and_labels in progress_bar(validation_data_loader, parent=mb):
                        mb.child.comment = 'Validation'

                        if vanilla_triplet_loss:
                            inputs = torch.cat(inputs_and_labels[:3])
                            labels = torch.cat(inputs_and_labels[3:])
                        else:
                            inputs, labels = inputs_and_labels

                        if device == 'cuda':
                            inputs = inputs.to(device)

                        outputs = model(inputs)

                        embeddings_val.append(outputs.cpu().detach())
                        labels_val.append(labels.cpu())

                    model = model.cpu()  # free memory for faiss

                    recall_at_5_eval = evaluate_recall_at_k(torch.cat(embeddings_val).numpy(), torch.cat(labels_val).numpy(), 5, 0, False)
                    extended_log = {'validation_recall': dict(zip(['@1', '@2', '@3', '@4', '@5'], recall_at_5_eval))}

                    if train_recall:
                        recall_at_5_train = np.array(running_batch_recall).mean(0)
                        extended_log.update({'train_avg_batch_recall': dict(zip(['@1', '@2', '@3', '@4', '@5'], recall_at_5_train))})

                    model = model.cuda()

            basic_log = {'epoch': epoch, 'train_loss': running_loss, 'learning_rate': learning_rate}
            basic_log.update(extended_log)
            run.log(basic_log)

            model.train()

            # Perform scheduler step (if necessary)
            if scheduler:
                learning_rate = optimizer.param_groups[0]['lr']
                scheduler.step(running_loss)

            # Save checkpoint after each epoch
            torch.save({
                'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict()
            }, checkpoint_path)


def train_model_with_mlp(model, experiment_name, training_data_loader, validation_data_loader,
                         loss, optimizer, learning_rate, epochs, scheduler=None, device='cpu',
                         use_checkpoint=True, checkpoint_folder='',
                         unfreeze_weights_at_iter=None, eval_period=1):
    """
    Train the backbone with an appended mlp for pairwise image classification for the specified amount of epochs.

    @param model: Model to be trained.
    @param training_data_loader: DataLoader for the training data.
    @param validation_data_loader: DataLoader for the validation data.
    @param loss: Loss function used for the training.
    @param optimizer: Optimizer used during training.
    @param learning_rate: Learning rate used during training.
    @param epochs: Number of epochs for which to train the model.
    @param scheduler: Scheduler used to tune the learning rate during training.
    @param device: Device on which to load the data and the model (CPU vs. GPU).
    @param use_checkpoint: Set to true if the training run should start at the last saved checkpoint (if it exists).
    @param checkpoint_folder: Folder in which to store the checkpoint file.
    @param unfreeze_weights_at_iter: Specifies in which epoch the backbone weights are unfrozen.
    @param eval_period:
    """

    # Create experiment name
    backbone_name = model.backbone.__class__.__name__
    loss_name = loss.__class__.__name__
    optimizer_name = optimizer.__class__.__name__
    scheduler_name = scheduler.__class__.__name__ if scheduler else 'NoScheduler'
    checkpoint_path = os.path.join(checkpoint_folder, f'{experiment_name}_checkpoint.pt')

    # os.environ['WANDB_MODE'] = 'offline'
    wandb.login(key="")
    with wandb.init(project="dll-experiments", entity="dll-art", resume=True, name=experiment_name,
                    tags=[backbone_name, loss_name, optimizer_name, scheduler_name, str(learning_rate)]) as run:

        # wandb watches gradients and parameters
        wandb.watch(model, log="all", log_freq=100)

        # Run information
        run.config.learning_rate = learning_rate
        run.config.optimizer = optimizer_name
        run.watch(model)

        # Set model to training mode
        if device == 'cuda':
            model.to(device)
        model.train()

        # Check if checkpoint exists. If it does, load it.
        start_epoch = 1
        if os.path.isdir(checkpoint_folder):
            if os.path.isfile(checkpoint_path) and use_checkpoint:
                checkpoint = torch.load(checkpoint_path)
                model.load_state_dict(checkpoint['model_state_dict'])
                optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
                start_epoch = checkpoint['epoch'] + 1
                learning_rate = optimizer.param_groups[0]['lr']
        else:
            os.makedirs(checkpoint_folder)

        mb = master_bar(range(start_epoch, epochs + 1))
        for epoch in mb:

            # Unfreeze backbone weights if necessary
            if epoch >= unfreeze_weights_at_iter and model.frozen:
                model.unfreeze_weights()

            # Here 'inputs' can refer to a list of single images, pairs, or triples (depending on loss function)
            n_minibatches = 0
            running_loss = 0
            true = []
            pred = []
            for img1, img2, labels in progress_bar(iter(training_data_loader), parent=mb):

                # Move batch to GPU (if it is available)
                if device == 'cuda':
                    img1, img2, labels = img1.to(device), img2.to(device), labels.to(device)

                # Set gradients to zero
                optimizer.zero_grad(set_to_none=True)

                # Perform the forward pass
                outputs = model.forward_pair(img1, img2)

                # Compute batch loss and backpropagate
                batch_loss = loss(outputs, labels)
                batch_loss.backward()

                # Perform optimization step
                optimizer.step()

                # Update predicted and true labels for later statistics
                true += labels.tolist()
                pred += torch.round(torch.sigmoid(outputs)).tolist()

                # Update running loss
                n_minibatches += 1
                running_loss += batch_loss.item()

            # Only log statistics every eval_period epochs
            if epoch % eval_period == 0 or epoch == epochs:
                model.eval()
                running_loss = running_loss / n_minibatches
                train_accuracy = accuracy_score(true, pred)
                train_precisions, train_recalls, train_f1, _ = precision_recall_fscore_support(true, pred,
                                                                                               average=None)  # per class precisions, recalls and F1-scores
                true_val, pred_val, val_loss = test_model(model, validation_data_loader, loss, 'sigmoid', True)
                val_accuracy = accuracy_score(true_val, pred_val)
                val_precisions, val_recalls, val_f1, _ = precision_recall_fscore_support(true_val, pred_val,
                                                                                         average=None)
                run.log({
                    'epoch': epoch,
                    'learning_rate': learning_rate,
                    "train": {'accuracy': train_accuracy,
                              'loss': running_loss,
                              'precision': {'0': train_precisions[0],
                                            '1': train_precisions[1]
                                            },
                              'recall': {'0': train_recalls[0],
                                         '1': train_recalls[1]
                                         },
                              'f1': {'0': train_f1[0],
                                     '1': train_f1[1]
                                     }
                              },
                    "val": {'accuracy': val_accuracy,
                            'loss': val_loss,
                            'precision': {'0': val_precisions[0],
                                          '1': val_precisions[1]
                                          },
                            'recall': {'0': val_recalls[0],
                                       '1': val_recalls[1]
                                       },
                            'f1': {'0': val_f1[0],
                                   '1': val_f1[1]
                                   }
                            }
                })

            model.train()

            # Perform scheduler step (if necessary)
            if scheduler:
                if isinstance(scheduler, ReduceLROnPlateau):
                    learning_rate = optimizer.param_groups[0]['lr']
                    scheduler.step(running_loss)
                else:
                    learning_rate = scheduler.get_last_lr()[-1]
                    scheduler.step()

                # Save checkpoint after each epoch
            torch.save({
                'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict()
            }, checkpoint_path)


def train_mlp(model, experiment_name, training_data_loader, validation_data_loader, loss, optimizer, learning_rate,
              epochs, scheduler=None, device='cpu', use_checkpoint=True, checkpoint_folder=''):
    """
       Training a binary classifier if two images are from the same painter or not.

       @param model: MLP to be trained.
       @param experiment_name:
       @param training_data_loader: DataLoader for the training data.
       @param validation_data_loader: DataLoader for the validation data.
       @param loss: Loss function used for the training.
       @param optimizer: Optimizer used during training.
       @param learning_rate: Learning rate used during training.
       @param epochs: Number of epochs for which to train the model.
       @param scheduler: Scheduler used to tune the learning rate during training.
       @param device: Device on which to load the data and the model (CPU vs. GPU).
       @param use_checkpoint: Set to true if the training run should start at the last saved checkpoint (if it exists).
       @param checkpoint_folder: Folder in which to store the checkpoint file.
       """
    # Create experiment name
    experiment_name += "_mlp"
    mlp_name = model.__class__.__name__
    loss_name = loss.__class__.__name__
    optimizer_name = optimizer.__class__.__name__
    scheduler_name = scheduler.__class__.__name__ if scheduler else 'NoScheduler'
    checkpoint_path = os.path.join(checkpoint_folder, f'{experiment_name}_checkpoint.pt')

    # os.environ['WANDB_MODE'] = 'offline'
    wandb.login(key="")
    with wandb.init(project="dll-experiments", entity="dll-art", resume=True, name=experiment_name,
                    tags=[mlp_name, loss_name, optimizer_name, scheduler_name, str(learning_rate)]) as run:
        # wandb watches gradients and parameters
        wandb.watch(model, log="all", log_freq=8)

        # Run information
        run.config.learning_rate = learning_rate
        run.config.optimizer = optimizer_name
        run.watch(model)

        # Set model to training mode
        model.train()
        if device == 'cuda':
            model.to(device)

        # Check if checkpoint exists. If it does, load it.
        start_epoch = 1
        if os.path.isdir(checkpoint_folder):
            if os.path.isfile(checkpoint_path) and use_checkpoint:
                checkpoint = torch.load(checkpoint_path)
                model.load_state_dict(checkpoint['model_state_dict'])
                optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
                start_epoch = checkpoint['epoch'] + 1
                learning_rate = optimizer.param_groups[0]['lr']
        else:
            os.makedirs(checkpoint_folder)

        mb = master_bar(range(start_epoch, epochs + 1))
        for epoch in mb:
            n_minibatches = 0
            running_loss = 0
            true = []
            pred = []
            for inputs, labels in progress_bar(iter(training_data_loader), parent=mb):
                # Move batch to GPU (if it is available)
                if device == 'cuda':
                    inputs, labels = inputs.to(device), labels.to(device)
                # Set gradients to zero
                optimizer.zero_grad(set_to_none=True)
                # Perform the forward pass
                outputs = model(inputs).squeeze()
                # Compute batch loss and backpropagate
                batch_loss = loss(outputs, labels)
                batch_loss.backward()
                # Perform optimization step
                optimizer.step()
                # Update predicted and true labels for later statistics
                true += labels.tolist()
                pred += torch.round(torch.sigmoid(outputs)).tolist()
                # Update running loss
                n_minibatches += 1
                running_loss += batch_loss.item()

            model.eval()
            running_loss = running_loss / n_minibatches
            train_accuracy = accuracy_score(true, pred)
            train_precisions, train_recalls, train_f1, _ = precision_recall_fscore_support(true, pred,
                                                                                           average=None)  # per class precisions, recalls and F1-scores
            true_val, pred_val, val_loss = test_model(model, validation_data_loader, loss, 'sigmoid')
            val_accuracy = accuracy_score(true_val, pred_val)
            val_precisions, val_recalls, val_f1, _ = precision_recall_fscore_support(true_val, pred_val, average=None)
            run.log({
                'epoch': epoch,
                'learning_rate': learning_rate,
                "train": {'accuracy': train_accuracy,
                          'loss': running_loss,
                          'precision': {'0': train_precisions[0],
                                        '1': train_precisions[1]
                                        },
                          'recall': {'0': train_recalls[0],
                                     '1': train_recalls[1]
                                     },
                          'f1': {'0': train_f1[0],
                                 '1': train_f1[1]
                                 }
                          },
                "val": {'accuracy': val_accuracy,
                        'loss': val_loss,
                        'precision': {'0': val_precisions[0],
                                      '1': val_precisions[1]
                                      },
                        'recall': {'0': val_recalls[0],
                                   '1': val_recalls[1]
                                   },
                        'f1': {'0': val_f1[0],
                               '1': val_f1[1]
                               }
                        }
            })

            model.train()

            # Perform scheduler step (if necessary)
            if scheduler:
                if isinstance(scheduler, ReduceLROnPlateau):
                    learning_rate = optimizer.param_groups[0]['lr']
                    scheduler.step(running_loss)
                else:
                    learning_rate = scheduler.get_last_lr()[-1]
                    scheduler.step()

            # Save checkpoint after each epoch
            torch.save({
                'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict()
            }, checkpoint_path)
