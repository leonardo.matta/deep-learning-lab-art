import numpy as np


class ClassBalancedBatchSampler(object):
    """
    BatchSampler that ensures a fixed amount of images per class are sampled in the minibatch
    """
    def __init__(self, targets, batch_size, images_per_class, replace=False, ignore_index=None):
        self.targets = targets
        self.batch_size = batch_size
        self.images_per_class = images_per_class
        self.ignore_index = ignore_index
        self.reverse_index, self.ignored = self._build_reverse_index()
        self.replace = replace

    def __iter__(self):
        for _ in range(len(self)):
            yield self.sample_batch()

    def _build_reverse_index(self):
        reverse_index = {}
        ignored = []
        for idx, label in self.targets.items():
            if idx == self.ignore_index:
                ignored.append(idx)
                continue
            if label not in reverse_index:
                reverse_index[label] = []
            reverse_index[label].append(idx)
        return reverse_index, ignored

    def sample_batch(self):
        num_classes = self.batch_size // self.images_per_class
        sampled_classes = np.random.choice(list(self.reverse_index.keys()),
                                           num_classes,
                                           replace=False)
        sampled_indices = []
        for cls in sampled_classes:
            # Need replace = True for datasets with non-uniform distribution of images per class
            replace = False
            if len(self.reverse_index[cls]) < self.images_per_class or self.replace:
                replace = True

            sampled_indices.extend(np.random.choice(self.reverse_index[cls],
                                                    self.images_per_class,
                                                    replace=replace))
        return sampled_indices

    def __len__(self):
        return len(self.targets) // self.batch_size
