# MIT License

# Copyright (c) Microsoft Corporation. All rights reserved.
# Copyright (c) 2022 Alexander Ermolov

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE

import torch
import torch.nn as nn
import torch.nn.functional as F
from hyptorch.pmath import dist_matrix


class NormSoftmaxLoss(nn.Module):
    """
    L2 normalize weights and apply temperature scaling on logits.
    """

    def __init__(self, temperature=0.05):
        super(NormSoftmaxLoss, self).__init__()

        self.temperature = temperature
        self.loss_fn = nn.CrossEntropyLoss()

    def forward(self, x, y):
        loss = self.loss_fn(x / self.temperature, y)

        return loss


class HypContrastiveLoss(nn.Module):
    """
    Hyperbolic pairwise cross-entropy loss from "Hyperbolic Vision Transformers: Combining Improvements in Metric Learning"
    This module is inspired by the code in the paper's repository: https://github.com/htdt/hyp_metric
    """

    def __init__(self, temperature=0.02, hyp_c=0.1, n_samples=2, device='cuda'):
        super(HypContrastiveLoss, self).__init__()

        self.temperature = temperature
        self.hyp_c = hyp_c
        self.n_samples = n_samples
        self.device = device

    def forward(self, x, y):

        # Reshape tensor so that
        x = x.view(len(x) // self.n_samples, self.n_samples, -1)

        # Compute a final loss summand for each positive pair in the batch
        total_loss = 0
        for i in range(self.n_samples):
            for j in range(self.n_samples):
                if i != j:
                    x0, x1 = x[:, i], x[:, j]
                    batch_size = x0.shape[0]

                    # Indices of the logits in the nominator of the cross-entropy function
                    target = torch.arange(batch_size).to(self.device)

                    # Compute logits for all samples and divide them by the temperature.
                    # Set logit of sample to itself as a very small number, effectively excluding it from the sum in the loss' denominator
                    logits_0 = -dist_matrix(x0, x1, self.hyp_c) / self.temperature
                    logits_1 = -dist_matrix(x0, x0, self.hyp_c) / self.temperature - torch.eye(batch_size,
                                                                                               device=self.device) * 1e9
                    logits = torch.cat([logits_0, logits_1], dim=1)

                    # Subtract the maximum logit to avoid numerical problems
                    logits -= logits.max(dim=1, keepdim=True)[0].detach()

                    total_loss += F.cross_entropy(logits, target)

        return total_loss


class TripletLoss(nn.TripletMarginLoss):
    """
    Class that inherits from torch.nn.TripletMarginLoss to ease usability with the miner.
    """

    def __init__(self, margin=1.0, p=2.0, eps=1e-06, swap=False, size_average=None, reduce=None, reduction='mean'):
        super().__init__(margin, p, eps, swap, size_average, reduce, reduction)

    def forward(self, embeddings, indices, **kwargs):
        anchor_idx, positive_idx, negative_idx = indices
        if len(anchor_idx) == 0:
            return torch.sum(embeddings * 0)
        return super().forward(embeddings[anchor_idx], embeddings[positive_idx], embeddings[negative_idx])


class TripletMarginLoss(nn.Module):
    """ This class is based on https://github.com/KevinMusgrave/pytorch-metric-learning.
        @param margin: The desired difference between the anchor-positive distance and the anchor-negative distance.
        @param swap: Use the positive-negative distance instead of anchor-negative distance, if it violates the margin more.
        @param soft_margin: Use the log-exp version of the triplet loss.
        @param reduce: Options are: 'mean', 'non_zero_mean' or 'sum'. 'non_zero_mean' computes the mean based only on triplets that contribute to the loss.
    """

    def __init__(self, margin=1.0, swap=False, soft_margin=False, reduce='mean'):
        super().__init__()
        self.margin = margin
        self.swap = swap
        self.soft_margin = soft_margin
        self.reduce = reduce

    def zero_loss(self, embeddings):
        return torch.sum(embeddings * 0)

    def forward(self, embeddings, indices):
        anchor_idx, positive_idx, negative_idx = indices
        if anchor_idx.size(0) == 0:
            return self.zero_loss(embeddings)

        mat = torch.cdist(embeddings, embeddings)
        ap_dists = mat[anchor_idx, positive_idx]
        an_dists = mat[anchor_idx, negative_idx]

        if self.swap:
            pn_dists = mat[positive_idx, negative_idx]
            an_dists = torch.min(an_dists, pn_dists)

        current_margins = ap_dists - an_dists
        violation = current_margins + self.margin

        if self.soft_margin:
            loss = torch.nn.functional.softplus(violation)
        else:
            loss = torch.nn.functional.relu(violation)

        if self.reduce == 'sum':
            loss = torch.sum(loss)
        else:
            if self.reduce == 'non_zero_mean':
                non_zero_condition = loss > 0
                if torch.count_nonzero(non_zero_condition) < 1:
                    return self.zero_loss(embeddings)
                loss = loss[non_zero_condition]
            loss = torch.mean(loss)

        return loss

