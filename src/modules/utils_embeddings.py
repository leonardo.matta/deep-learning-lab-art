import os

import torch
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm
import numpy as np
from torch import nn


class EmbeddingGenerator(object):
    """Helper class to compute, save and visualize embeddings.
    @param model: a trained model that returns a N-dimensional tensor e.g. for our ResNet: model.fc = torch.nn.modules.Identity().
    @param img_dataset: EmbeddingDataset
    @param batch_size: Batch size for computing only the forward pass.
    @param img_out_dir: directory where images are saved on small scale for retrieval with tensorboard.
    @param emb_dir:  directory where embeddings are saved.
    @param tensorboard_dir: directory to write tensorboard files.
    @param experiment_name: corresponding experiment name.
    @param device: cpu or cuda.
    @param num_workers: number of parallel workers for the DataLoader.
    """
    def __init__(self, model, img_dataset, batch_size, img_out_dir, emb_dir, tensorboard_dir, experiment_name,
                 device='cpu', num_workers=0):
        self.model = model
        if device == 'cuda':
            self.model.cuda()
        self.img_dataset = img_dataset
        self.batch_size = batch_size
        self.img_out_dir = img_out_dir
        self.emb_dir = emb_dir
        self.tensorboard_dir = tensorboard_dir
        self.experiment_name = experiment_name
        self.device = device
        self.num_workers = num_workers
        self.writer = None
        self.global_step = 0

    def disable_dense_layer(func):
        """
        Decorator that disables the dense layer of the model. The model's forward pass will then return the input's embeddings.
        """
        def wrapper(*args, **kwargs):
            disable = True
            if "disable" in kwargs:
                disable = kwargs.get("disable")
            if disable:
                self = args[0]
                fc = self.model.fc
                self.model.fc = nn.Identity()
                res = func(*args, **kwargs)
                self.model.fc = fc
            else:
                res = func(*args, **kwargs)
            return res
        
        return wrapper

    @disable_dense_layer
    def store_embeddings(self, embeddings_only=True, img_out_size=(32, 32), disable=True):
        """
        Stores embeddings as Torch tensors in single .pt files in the corresponding directory. With the embedding the corresponding label is also saved as a tuple.
        Also stores the corresponding images as Torch tensors if necessary, by applying a resize.
        @param embeddings_only: Specifies if only embeddings are saved, or also the corresponding images are resized and stored. These are needed to create the sprite image for annotation.
        @param img_out_size: The requested size in pixels, as a 2-tuple: (width, height).
        @param disable: Parameter for the decorator if the classification layer is replaced by an Identity layer or not
        """
        path_write_emb = os.path.join(self.emb_dir, self.experiment_name)

        if not os.path.isdir(self.img_out_dir):
            os.makedirs(self.img_out_dir)

        if not os.path.isdir(path_write_emb):
            os.makedirs(path_write_emb)

        loader = DataLoader(self.img_dataset, self.batch_size, shuffle=False, num_workers=self.num_workers)
        with torch.no_grad():
            for imgs, labels, file_names in tqdm(iter(loader)):
                if self.device == 'cuda':
                    imgs = imgs.to(self.device)
                embeddings = self.model(imgs)
                embeddings = embeddings.cpu()
                

                if not embeddings_only:
                    imgs = imgs.cpu()
                    resized_imgs = torch.nn.functional.interpolate(imgs, img_out_size)

                names = [name.split('.')[0] for name in file_names]
                for i in range(len(imgs)):
                    torch.save((embeddings[i].clone(), labels[i].clone()),
                               os.path.join(path_write_emb, names[i] + '.pt'))

                    if not embeddings_only:
                        torch.save(resized_imgs[i].clone(), os.path.join(self.img_out_dir, names[i] + '.pt'))

    @disable_dense_layer
    def compute_embeddings(self, disable=True):
        """
        Compute image embeddings without storing them in the file system.
        """
        loader = DataLoader(self.img_dataset, self.batch_size, shuffle=False, num_workers=self.num_workers)
        embeddings_list = []
        labels_list = []
        filenames_list = []
        with torch.no_grad():
            for imgs, labels, filenames in tqdm(iter(loader)):
                if self.device == 'cuda':
                    imgs = imgs.to(self.device)
                embeddings = self.model(imgs)
                embeddings = embeddings.cpu()
                embeddings_list += list(embeddings)
                labels_list += labels.tolist()
                filenames_list += filenames

        return embeddings_list, labels_list, filenames_list
                

    @disable_dense_layer
    def write_to_tensorboard_log(self, max_len, without_image_labels=False, disable=True):
        """
        Writes the stored embedding (and image) data to the tensorboard log.
        @param max_len: Maximum number of embeddings per tensorlog file. This number corresponds to the amount of embeddings that can be displayed together at once.
        @param without_image_labels: Flag, to show embeddings only without corresponding images.
        @param disable: Parameter for the decorator if the classification layer is replaced by an Identity layer or not.
        """
        path_to_tensorboard_dir = os.path.join(self.emb_dir, self.experiment_name, self.tensorboard_dir)
        if not os.path.isdir(path_to_tensorboard_dir):
            os.makedirs(path_to_tensorboard_dir)

        if self.writer is None:
            self.writer = SummaryWriter(path_to_tensorboard_dir)

        path_emb = os.path.join(self.emb_dir, self.experiment_name)

        embeddings = []
        images = []
        labels = []

        step = self.global_step
        last_iteration = len(os.listdir(path_emb)) - 1
        for i, filename in enumerate(tqdm(os.listdir(path_emb))):
            if (i > 0 and i % max_len == 0) or i == last_iteration:
                embeddings = torch.stack(embeddings)
                if without_image_labels:
                    self.writer.add_embedding(embeddings, metadata=labels, global_step=step)
                else:
                    images = torch.stack(images)
                    self.writer.add_embedding(embeddings, metadata=labels, label_img=images, global_step=step)
                    images = []
                step += 1
                embeddings = []
                labels = []

            if filename.endswith('.pt'):
                data = torch.load(os.path.join(path_emb, filename))
                embeddings.append(data[0])
                labels.append(data[1].tolist())
            if without_image_labels:
                continue
            if filename in os.listdir(self.img_out_dir):
                images.append(torch.load(os.path.join(self.img_out_dir, filename)))
        self.global_step = step
        self.writer.flush()

    @disable_dense_layer
    def compute_and_log(self, img_out_size=None, without_image_labels=False, disable=True):
        """
        Computes the embeddings and logs them directly into the tensorboard file, not intended for use with the complete dataset due to excessive memory usage.
        For a large number of images resizing is necessary. Otherwise the sprite image can't be loaded in the tensoboard embedding visualition.

        @param img_out_size: If provided the requested size in pixels, as a 2-tuple: (width, height). Otherwise no resizing applied.
        @param without_image_labels: Flag to show embeddings without corresponding images.
        @param disable: Parameter for the decorator if the classification layer is replaced by an Identity layer or not.
        """
        if not os.path.isdir(os.path.join(self.emb_dir, self.experiment_name)):
            os.makedirs(os.path.join(self.emb_dir, self.experiment_name))

        path_to_tensorboard_dir = os.path.join(self.emb_dir, self.experiment_name, self.tensorboard_dir)
        if not os.path.isdir(path_to_tensorboard_dir):
            os.mkdir(path_to_tensorboard_dir)

        loader = DataLoader(self.img_dataset, self.batch_size, shuffle=False, num_workers=self.num_workers)

        all_embeddings = []
        all_labels = []
        all_imgs = []

        with torch.no_grad():
            for imgs, labels, file_names in tqdm(iter(loader)):
                if self.device == 'cuda':
                    imgs = imgs.to(self.device)

                embeddings = self.model(imgs)

                embeddings = embeddings.cpu()

                all_embeddings.append(embeddings)
                all_labels += labels.tolist()

                if without_image_labels:
                    continue

                imgs = imgs.cpu()

                if img_out_size:
                    all_imgs.append(torch.nn.functional.interpolate(imgs, img_out_size))
                    continue

                all_imgs.append(imgs)

            all_embeddings = torch.cat(all_embeddings)

            if not without_image_labels:
                all_imgs = torch.cat(all_imgs)

            if self.writer is None:
                self.writer = SummaryWriter(path_to_tensorboard_dir)

            if without_image_labels:
                self.writer.add_embedding(all_embeddings, all_labels, global_step=self.global_step)
            else:
                self.writer.add_embedding(all_embeddings, metadata=all_labels, label_img=all_imgs, global_step=self.global_step)
            self.global_step += 1
            self.writer.flush()

    def load_embeddings(self):
        """
        Loads the saved embeddings.
        """

        path_read_emb = os.path.join(self.emb_dir, self.experiment_name)

        embeddings = []
        labels = []
        filenames = os.listdir(path_read_emb)
        for file in filenames:
            emb, label = torch.load(os.path.join(path_read_emb, file))
            embeddings.append(emb.numpy())
            labels.append(label.item())

        return np.array(embeddings), np.array(labels)