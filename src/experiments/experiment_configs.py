from functools import partial

import torch
import timm
from torch.nn import CrossEntropyLoss, BCEWithLogitsLoss
from torch.optim import Adam, AdamW, SGD
from torch.optim.lr_scheduler import ReduceLROnPlateau, StepLR, MultiStepLR
from torchvision import transforms
from torchvision.models.efficientnet import efficientnet_v2_s
from torchvision.models.resnet import resnet50

from ..modules.data import ClassificationLossDataset, TripletLossDataset, BCELossDataset, BCELossMLPDataset
from ..modules.losses import NormSoftmaxLoss, HypContrastiveLoss, TripletMarginLoss, TripletLoss
from ..modules.miner import TripletMarginMiner
from ..modules.models import DropoutFeaturizer, L2NormalizedDenseLayer, L2NormalizedFeaturizer
from ..modules.models import HypDenseLayer, LayerNormFeaturizer, TripletEmbeddingLayer



"""
Each entry in the following dictionary represents the parameters of an experiment.
"""
experiment_configs = {
    # We first train a simple ResNet50 base model with Adam
    'classification_loss_1': {'model_params': {'backbone': resnet50},
                              'dataset': ClassificationLossDataset,
                              'loss': CrossEntropyLoss,
                              'loss_params': {},
                              'optimizer': Adam,
                              'optimizer_params': {},
                              'learning_rate': 5e-4,
                              'scheduler': None,
                              'scheduler_params': {},
                              'augmentation': None,
                              'transform': transforms.Compose([transforms.RandomCrop(256), transforms.ToTensor()]),
                              'batch_size': 32,
                              'sampler_params': None,
                              'epochs': 20,
                              'unfreeze_iter': None,
                              'gradient_clipping_value': None
                              },
    # We add a dropout layer before the fully convolutional layer
    'classification_loss_2': {'model_params': {'backbone': resnet50, 'dropout_p': 0.5, 'featurizer': DropoutFeaturizer},
                              'dataset': ClassificationLossDataset,
                              'loss': CrossEntropyLoss,
                              'loss_params': {},
                              'optimizer': AdamW,
                              'optimizer_params': {},
                              'learning_rate': 5e-4,
                              'scheduler': None,
                              'scheduler_params': {},
                              'augmentation': None,
                              'transform': transforms.Compose([transforms.RandomCrop(256), transforms.ToTensor()]),
                              'batch_size': 32,
                              'sampler_params': None,
                              'epochs': 20,
                              'unfreeze_iter': None,
                              'gradient_clipping_value': None
                              },
    # We use the implementation of the paper "Classification is a Strong Baseline for Deep Metric Learning"
    'classification_loss_3': {'model_params': {'backbone': resnet50, 'featurizer': LayerNormFeaturizer,
                                               'denseLayer': L2NormalizedDenseLayer, 'fix_block_weights': True},
                              'dataset': ClassificationLossDataset,
                              'loss': NormSoftmaxLoss,
                              'loss_params': {},
                              'optimizer': Adam,
                              'optimizer_params': {},
                              'learning_rate': 3e-4,
                              'scheduler': None,
                              'scheduler_params': {},
                              'augmentation': None,
                              'transform': transforms.Compose([transforms.RandomCrop(256), transforms.ToTensor()]),
                              'batch_size': 96,
                              'sampler_params': None,
                              'epochs': 30,
                              'unfreeze_iter': 2
                              },
    # We test input normalization using a batch normalization layer
    'classification_loss_4': {'model_params': {'backbone': resnet50, 'featurizer': LayerNormFeaturizer,
                                               'denseLayer': L2NormalizedDenseLayer, 'fix_block_weights': True,
                                               'input_norm': True},
                              'dataset': ClassificationLossDataset,
                              'loss': NormSoftmaxLoss,
                              'loss_params': {},
                              'optimizer': Adam,
                              'optimizer_params': {},
                              'learning_rate': 3e-4,
                              'scheduler': None,
                              'scheduler_params': {},
                              'transform': transforms.Compose([transforms.RandomCrop(256),
                                                               transforms.ToTensor()]),
                              'batch_size': 96,
                              'sampler_params': None,
                              'epochs': 40,
                              'unfreeze_iter': None,
                              'gradient_clipping_value': None
                              },
    # We try to improve our results by using a different optimizer (SDG) and a learning rate scheduler
    'classification_loss_5': {'model_params': {'backbone': resnet50, 'featurizer': LayerNormFeaturizer,
                                               'denseLayer': L2NormalizedDenseLayer, 'fix_block_weights': True},
                              'dataset': ClassificationLossDataset,
                              'loss': NormSoftmaxLoss,
                              'loss_params': {},
                              'optimizer': SGD,
                              'optimizer_params': {'momentum': 0.9},
                              'learning_rate': 0.01,
                              'scheduler': ReduceLROnPlateau,  # difference to Exp3
                              'scheduler_params': {'mode': 'max'},  # difference to Exp3
                              'transform': transforms.Compose([transforms.RandomCrop(256), transforms.ToTensor()]),
                              'batch_size': 96,
                              'sampler_params': {'images_per_class': 4, 'replace': False},
                              'epochs': 80,
                              'unfreeze_iter': 2
                              },
    # We try to improve the model's generalization by including data augmentation
    'classification_loss_6': {'model_params': {'backbone': resnet50, 'featurizer': LayerNormFeaturizer,
                                               'denseLayer': L2NormalizedDenseLayer, 'fix_block_weights': True,
                                               'input_norm': False},
                              'dataset': ClassificationLossDataset,
                              'loss': NormSoftmaxLoss,
                              'loss_params': {},
                              'optimizer': SGD,
                              'optimizer_params': {'momentum': 0.9, 'weight_decay': 1e-4},
                              'learning_rate': 0.01,
                              'scheduler': ReduceLROnPlateau,
                              'scheduler_params': {'mode': 'max'},
                              # Imagenet standards
                              'transform': transforms.Compose([transforms.RandomCrop(256),
                                                               transforms.ToTensor(),
                                                               transforms.RandomRotation(degrees=15),
                                                               transforms.ColorJitter(),
                                                               transforms.RandomHorizontalFlip()]),
                              'batch_size': 96,
                              'sampler_params': {'images_per_class': 4, 'replace': False},
                              'epochs': 80,
                              'unfreeze_iter': 2
                              },
    # We change the backbone 
    'classification_loss_7': {'model_params': {'backbone': efficientnet_v2_s, 'featurizer': LayerNormFeaturizer,
                                               'denseLayer': L2NormalizedDenseLayer, 'fix_block_weights': True,
                                               'input_norm': True},
                              'dataset': ClassificationLossDataset,
                              'loss': NormSoftmaxLoss,
                              'loss_params': {},
                              'optimizer': SGD,
                              'optimizer_params': {'momentum': 0.9, 'weight_decay': 1e-4},
                              'learning_rate': 0.01,
                              'scheduler': MultiStepLR,
                              'scheduler_params': {'milsetones': [13]},
                              # Imagenet standards
                              'transform': transforms.Compose([transforms.RandomCrop(256),
                                                               transforms.ToTensor(),
                                                               transforms.RandomRotation(degrees=15),
                                                               transforms.ColorJitter(),
                                                               transforms.RandomHorizontalFlip()]),
                              'batch_size': 56,
                              'sampler_params': {'images_per_class': 4, 'replace': False},
                              'epochs': 80,
                              'unfreeze_iter': 2,
                              'gradient_clipping_value': None
                              },
    # Transformer backbone
    'classification_loss_8': {'model_params': {'backbone': partial(timm.create_model, 'vit_small_patch16_224'),
                                              'featurizer': LayerNormFeaturizer, 'denseLayer': L2NormalizedDenseLayer, 'fix_block_weights': False,
                                              'input_norm': False},
                             'dataset': ClassificationLossDataset,
                             'loss': NormSoftmaxLoss,
                             'loss_params': {},
                             'optimizer': AdamW,
                             'optimizer_params': {'weight_decay': 1e-2},
                             'learning_rate': 3e-5,
                             'scheduler': None,
                             'scheduler_params': {},
                             'transform': transforms.Compose([transforms.Resize(224),
                                                               transforms.RandomCrop(224),
                                                               transforms.ToTensor(),
                                                               transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
                                                               transforms.RandomRotation(degrees=15),
                                                               transforms.ColorJitter(),
                                                               transforms.RandomHorizontalFlip()]),
                             'batch_size': 96,
                             'sampler_params': {'images_per_class': 4, 'replace': False},
                             'epochs': 200,
                             'unfreeze_iter': 0,
                             'gradient_clipping_value': None
    },
    # Contrastive loss
    'contrastive_loss_1': {'model_params': {'backbone': partial(timm.create_model, 'vit_small_patch16_224'),
                                            'featurizer': L2NormalizedFeaturizer, 'denseLayer': HypDenseLayer,
                                            'fix_block_weights': False,
                                            'input_norm': False},
                           'dataset': ClassificationLossDataset,
                           'loss': HypContrastiveLoss,
                           'loss_params': {'n_samples': 2},
                           'optimizer': AdamW,
                           'optimizer_params': {'weight_decay': 1e-2},
                           'learning_rate': 3e-5,
                           'scheduler': ReduceLROnPlateau,
                           'scheduler_params': {'mode': 'min'},
                           'transform': transforms.Compose([transforms.Resize(224),
                                                            transforms.RandomCrop(224),
                                                            transforms.ToTensor(),
                                                            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
                                                            transforms.RandomRotation(degrees=15),
                                                            transforms.ColorJitter(),
                                                            transforms.RandomHorizontalFlip()]),
                           'batch_size': 56,
                           'sampler_params': {'images_per_class': 2, 'replace': False},
                           'epochs': 200,
                           'unfreeze_iter': 0,
                           'gradient_clipping_value': 3
                           },
    'classification_loss_7_mlp': {'model_params': {'input_dim': 2560, 'hidden_dim': 4096},
                                  'dataset': BCELossMLPDataset,
                                  'loss': BCEWithLogitsLoss,
                                  'loss_params': {},
                                  'optimizer': AdamW,
                                  'optimizer_params': {},
                                  'learning_rate': 1e-3,
                                  'scheduler': StepLR,
                                  'scheduler_params': {'step_size': 14},
                                  'transform': None,
                                  'batch_size': 8192,
                                  'epochs': 30,
                                  },
    'classification_loss_7_mlp_intern': {
        'model_params': {'input_dim': 2560, 'hidden_dim': 4096},
        'dataset': BCELossDataset,
        'loss': BCEWithLogitsLoss,
        'loss_params': {},
        'optimizer': AdamW,
        'optimizer_params': {},
        'learning_rate': 1e-3,
        'scheduler': ReduceLROnPlateau,
        'scheduler_params': {'patience': 4},
        # Imagenet standards
        'transform': transforms.Compose([transforms.RandomCrop(256),
                                         transforms.ToTensor(),
                                         transforms.RandomRotation(degrees=15),
                                         transforms.ColorJitter(),
                                         transforms.RandomHorizontalFlip()]),
        'batch_size': 32,  # each batch contains twice as many images
        'sampler_params': None,
        'epochs': 40,
        'unfreeze_iter': 2,
        'checkpoint': 'classification_loss_7_checkpoint.pt',
        'main_key': 'classification_loss_7'
    },
    'triplet_loss_1': {'model_params': {'backbone': resnet50, 'output_dim': 1024, 'featurizer': None,
                                        'denseLayer': TripletEmbeddingLayer, 'fix_block_weights': False,
                                        'input_norm': False},
                       'dataset': ClassificationLossDataset,
                       'loss': TripletLoss,
                       'loss_params': {'margin': 0.3, 'swap': False},
                       'miner': TripletMarginMiner,
                       'miner_params': {'margin': 0.3, 'type_of_triplets': 'all', 'verbose': True},
                       'optimizer': AdamW,
                       'optimizer_params': {},
                       'learning_rate': 1e-4,
                       'scheduler': ReduceLROnPlateau,
                       'scheduler_params': {'threshold': 5e-4},
                       # Imagenet standards
                       'transform': transforms.Compose([transforms.RandomCrop(256),
                                                        transforms.ToTensor(),
                                                        transforms.RandomRotation(degrees=15),
                                                        transforms.ColorJitter(),
                                                        transforms.RandomHorizontalFlip()]),
                       'batch_size': 88,
                       'sampler_params': {'images_per_class': 4, 'replace': True},
                       'epochs': 80,
                       'unfreeze_iter': 0,
                       'gradient_clipping_value': None,
                       },
    'triplet_loss_2': {'model_params': {'backbone': resnet50, 'output_dim': 1024, 'featurizer': None,
                                        'denseLayer': TripletEmbeddingLayer, 'fix_block_weights': False,
                                        'input_norm': False},
                       'dataset': ClassificationLossDataset,
                       'loss': TripletLoss,
                       'loss_params': {'margin': 0.3, 'swap': False},
                       'miner': TripletMarginMiner,
                       'miner_params': {'margin': 0.3, 'type_of_triplets': 'easy'},
                       'optimizer': AdamW,
                       'optimizer_params': {},
                       'learning_rate': 4e-4,
                       'scheduler': ReduceLROnPlateau,
                       'scheduler_params': {'threshold': 5e-4},
                       # Imagenet standards
                       'transform': transforms.Compose([transforms.RandomCrop(256),
                                                        transforms.ToTensor(),
                                                        transforms.RandomRotation(degrees=15),
                                                        transforms.ColorJitter(),
                                                        transforms.RandomHorizontalFlip()]),
                       'batch_size': 104,
                       'sampler_params': {'images_per_class': 4, 'replace': False},
                       'epochs': 80,
                       'unfreeze_iter': 0,
                       'gradient_clipping_value': None,
                       },
    # batch-hard triplet loss
    'triplet_loss_3': {'model_params': {'backbone': resnet50, 'output_dim': 1024, 'featurizer': None,
                                        'denseLayer': TripletEmbeddingLayer, 'fix_block_weights': False,
                                        'input_norm': False},
                       'dataset': ClassificationLossDataset,
                       'loss': TripletMarginLoss,
                       'loss_params': {'margin': 0.3, 'swap': False, 'reduce': 'mean'},
                       'miner': TripletMarginMiner,
                       'miner_params': {'margin': 0.3, 'type_of_triplets': 'batch-hard'},
                       'optimizer': AdamW,
                       'optimizer_params': {},
                       'learning_rate': 3e-4,
                       'scheduler': ReduceLROnPlateau,
                       'scheduler_params': {'threshold': 5e-4},
                       # Imagenet standards
                       'transform': transforms.Compose([transforms.RandomCrop(256),
                                                        transforms.ToTensor(),
                                                        transforms.RandomRotation(degrees=15),
                                                        transforms.ColorJitter(),
                                                        transforms.RandomHorizontalFlip()]),
                       'batch_size': 104,
                       'sampler_params': {'images_per_class': 4, 'replace': False},
                       'epochs': 80,
                       'unfreeze_iter': 0,
                       'gradient_clipping_value': None,
                       },
    # batch all #2 but with loss reduction and anchor swap
    'triplet_loss_4': {'model_params': {'backbone': resnet50, 'output_dim': 1024, 'featurizer': None,
                                        'denseLayer': TripletEmbeddingLayer, 'fix_block_weights': False,
                                        'input_norm': False},
                       'dataset': ClassificationLossDataset,
                       'loss': TripletMarginLoss,
                       'loss_params': {'margin': 0.3, 'swap': True, 'reduce': 'non_zero_mean'},
                       'miner': TripletMarginMiner,
                       'miner_params': {'margin': 0.3, 'type_of_triplets': 'all'},
                       'optimizer': AdamW,
                       'optimizer_params': {},
                       'learning_rate': 2e-4,
                       'scheduler': ReduceLROnPlateau,
                       'scheduler_params': {'threshold': 5e-4},
                       # Imagenet standards
                       'transform': transforms.Compose([transforms.RandomCrop(256),
                                                        transforms.ToTensor(),
                                                        transforms.RandomHorizontalFlip()]),
                       'batch_size': 104,
                       'sampler_params': {'images_per_class': 4, 'replace': False},
                       'epochs': 80,
                       'unfreeze_iter': 0,
                       'gradient_clipping_value': None,
                       },
    # TODO: batch all without batch sampler
    'triplet_loss_5': {'model_params': {'backbone': resnet50, 'output_dim': 1024, 'featurizer': None,
                                        'denseLayer': TripletEmbeddingLayer, 'fix_block_weights': False,
                                        'input_norm': False},
                       'dataset': ClassificationLossDataset,
                       'loss': TripletMarginLoss,
                       'loss_params': {'margin': 0.1, 'swap': True, 'reduce': 'non_zero_mean'},
                       'miner': TripletMarginMiner,
                       'miner_params': {'margin': 0.1, 'type_of_triplets': 'all'},
                       'optimizer': AdamW,
                       'optimizer_params': {},
                       'learning_rate': 1e-4,
                       'scheduler': ReduceLROnPlateau,
                       'scheduler_params': {'patience': 10, 'threshold': 2e-4, 'factor': 0.3},
                       # Imagenet standards
                       'transform': transforms.Compose([transforms.RandomCrop(256),
                                                        transforms.ToTensor(),
                                                        transforms.RandomHorizontalFlip()]),
                       'batch_size': 104,
                       'sampler_params': None,
                       'epochs': 80,
                       'unfreeze_iter': 0,
                       'gradient_clipping_value': None,
                       },
    # vanilla triplet loss for reference
    'triplet_loss_6': {'model_params': {'backbone': resnet50, 'output_dim': 1024, 'featurizer': None,
                                        'denseLayer': TripletEmbeddingLayer, 'fix_block_weights': False,
                                        'input_norm': False},
                       'dataset': TripletLossDataset,  # constructs a triplet for every image
                       'loss': torch.nn.TripletMarginLoss,
                       'loss_params': {'margin': 0.3, 'swap': True},
                       'miner': None,
                       'miner_params': {},
                       'optimizer': AdamW,
                       'optimizer_params': {},
                       'learning_rate': 7e-4,
                       'scheduler': ReduceLROnPlateau,
                       'scheduler_params': {'threshold': 1e-4},
                       # Imagenet standards
                       'transform': transforms.Compose([transforms.RandomCrop(256),
                                                        transforms.ToTensor(),
                                                        transforms.RandomRotation(degrees=15),
                                                        transforms.ColorJitter(),
                                                        transforms.RandomHorizontalFlip()]),
                       'batch_size': 40,
                       'sampler_params': {'images_per_class': 4, 'replace': False},
                       'epochs': 80,
                       'unfreeze_iter': 0,
                       'gradient_clipping_value': None,
                       }
}
